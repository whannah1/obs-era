#!/usr/bin/env python
# Script for downloading ERA5 pressure level and surface data
# links to CDS web interface:
#   https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels
#   https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels
# A list of available variables can also be found in the ERA5 documentation:
#   https://confluence.ecmwf.int/display/CKB/ERA5%3A+data+documentation
import os, cdsapi
server = cdsapi.Client()

monthly = True

yr_list   = [f'{i:04d}' for i in range(2005,2005+1)]
mn_list   = [f'{i:02d}' for i in range(1,12+1)]

output_path = '/global/cscratch1/sd/whannah/Obs/ERA5'


for yr in yr_list:
  for mn in mn_list:
    server.retrieve('reanalysis-era5-single-levels-monthly-means',{
        'product_type'  : 'monthly_averaged_reanalysis',
        'time'          : ['00:00'],
        'month'         : mn,
        'year'          : yr,
        'format'        : 'netcdf',
        'variable'      : [
                          'land_sea_mask',
                          # 'orography',
                          # 'sea_ice_cover',
                          ],
    }, f'{output_path}/monthly/ERA5.monthly.land-sea-mask.{yr}-{mn}.nc')


