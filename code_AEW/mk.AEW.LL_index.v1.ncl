load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
load "$NCARG_ROOT/custom_functions_ERAi.ncl"
begin
    
    yr = 2008

    ; months = (/7,8,9,10/)
    ; months = (/8,9/)
    months = 8

    min_speed = 6.
    max_speed = 11.

    B1_lat1 =   5;10
    B1_lat2 =  20
    B1_lon1 = -30
    B1_lon2 = -20

    B2_lat1 =  10
    B2_lat2 =  20
    B2_lon1 = -65
    B2_lon2 = -55


    plot_hov = True
    plot_map = True

    ; ilev = (/500,700,300/)
    ilev = (/500,700/)

    fig_type = "png"
    fig_file = "~/Research/Obs/ERAi/figs_AEW/AEW.LL_index.v1."+yr+""

;==================================================================================
;==================================================================================
    
    opt  = True
    ; opt@debug    = debug
    opt@monthly = False
    opt@daily_avg = True
    opt@lev   = -1

    opt@lon1  = -100.
    opt@lon2  =   0.
    opt@lat1  =   0.
    opt@lat2  =  40.

    opt@ERAIyr1  = yr
    opt@ERAIyr2  = yr

    ; olon1 = opt@lon1
    ; olon2 = opt@lon2
    ; if olon1.lt.0 then olon1 = olon1+360 end if
    ; if olon2.lt.0 then olon2 = olon2+360 end if


    nh = 1
    fca = 1./(10.*nh)   ; longer period
    fcb = 1./( 2.*nh)   ; shorter period
    opt = False       
    nwgt  = 12*nh+1
    sigma = 1.0
    wgtBP = filwgts_lanczos (nwgt,2,fca,fcb,sigma)   ; band pass
    wgtHP = filwgts_lanczos (nwgt,1,fcb,0,sigma)     ; high pass
    wgtLP = filwgts_lanczos (nwgt,0,fca,0,sigma)     ; low pass
;==================================================================================
; Load various datasets
;==================================================================================
    
    ;--------------------------------------
    ; Load IBTRACS data
    ;--------------------------------------
    ; ifile = "~/Data/Obs/IBTRACS/Basin.NA.ibtracs_all.v03r08.nc"
    ; infile = addfile(ifile,"r")
    ; print(infile)
    ; exit

    ; slat = infile->source_lat
    ; mlat = infile->lat_for_mapping
    ; exit

    ;--------------------------------------
    ; Load OLR data
    ;--------------------------------------
    ifile = "~/Data/Obs/OLR/olr.daily.2000-2012.nc"    
    infile = addfile(ifile,"r")

    ; ocal = cd_calendar(infile->time,0)
    ; val = ind( (ocal(:,0).eq.yr) .and. \
    ;            (ocal(:,1).eq.mn) .and. \
    ;            (ocal(:,2).eq.dy)  )

    ; OLR = infile->OLR(:,{opt@lat1:opt@lat2},:)
    ; OLR = flip_lon(OLR)
    ; tmp = OLR(:,{opt@lon1:opt@lon2})
    ; OLR := tmp
    
    ;--------------------------------------
    ; Load ERAi eddy vortcity
    ;--------------------------------------
    iPs = LoadERAi("PS" ,opt)
    iE  = LoadERAi("EVR" ,opt)

    ; Isolate season
    ecal = cd_calendar(iE&time,0)
    mn = ecal(:,1)
    if isvar("months") then
        condition := mn.eq.0
        do m = 0,dimsizes(months)-1 condition = condition.or.(mn.eq.months(m)) end do
        vals := where(condition,ispan(0,dimsizes(mn)-1,1),-1)
        if all(ismissing(vals)) then
            print("No values found for months:")
            print("  "+months)
            exit
        end if
        vals:= vals(ind(vals.ge.0))
        E  := iE (vals,:,:,:)
        Ps := iPs(vals,:,:)
    else
        E  := iE 
        Ps := iPs
    end if
    delete([/iE,iPs/])
    etime = E&time
    elev  = E&lev
    elat  = E&lat
    elon  = E&lon

    ntime = dimsizes(etime)
    nlat  = dimsizes(etime)
    nlon  = dimsizes(etime)
    
    ecal := cd_calendar(E&time,0)


    ;--------------------------------------
    ; plots for checking stuff
    ;--------------------------------------
    ; hE = dim_avg_n_Wrap(E(:,:,{10:20},:),2)

    ; val := ind( (ecal(:,1).eq.mn) .and. \
    ;             (ecal(:,2).eq.dy) .and. \
    ;             (ecal(:,3).eq.0)  )
    
    ; qplot_map( E(val,{500},:,:) )

    ; tmp := dim_avg_n_Wrap(Ps(:,{10:20},:),1)
    ; tmp = tmp - conform(tmp,dim_avg_n(tmp,0),1)
    ; qplot_contour_fill( tmp )
    ; exit

    ; dt = 10*4
    ; tmp := dim_avg_n_Wrap( X(val-dt:val+dt,{500},{15:25},{-100:0}) ,1)
    ; tmp&time = ecal(val-dt:val+dt,2) + tofloat(ecal(val-dt:val+dt,3))/24.
    ; qplot_contour_fill( tmp )
;==================================================================================
; Algorithm for wave identification begins here
;==================================================================================
    ;--------------------------------------
    ; Define boxes for initial guess
    ;--------------------------------------
    num_ilev = dimsizes(ilev)
    ; E1 = new((/ntime,num_ilev/),float) 
    
    E1 = dim_avg_n_Wrap( E(:,{ilev},{B1_lat1:B1_lat2},{B1_lon1:B1_lon2}),(/2,3/))
    E2 = dim_avg_n_Wrap( E(:,{ilev},{B2_lat1:B2_lat2},{B2_lon1:B2_lon2}),(/2,3/))

    B_distance = abs( avg((/B2_lon1,B2_lon2/)) - avg((/B1_lon1,B1_lon2/)) )
    if B_distance.gt.180 then B_distance = B_distance-180 end if

    B_distance = B_distance * 111e3
    
    ;--------------------------------------
    ; Find events in Box 1 
    ; - allow for 1-day slop in max timing
    ;--------------------------------------
    tvals0 = ispan(0,ntime-3,1)
    tvals1 = ispan(1,ntime-2,1)
    tvals2 = ispan(2,ntime-1,1)

    cond := new((/ntime,num_ilev/),logical)

    cond(tvals1,:) = E1(tvals1,:).gt.0. .and. \
                     E1(tvals1,:).ge.E1(tvals0,:) .and. \
                     E1(tvals1,:).ge.E1(tvals2,:)
    if .not.any(cond) then printExit("No Maxima found in Box 1") end if
    
    if num_ilev.gt.1 then 
        B1_vals := ind( cond(:,0) );+1
        do i = 0,dimsizes(B1_vals)-1
        do k = 1,num_ilev-1
            dt = 1
            ii = B1_vals(i)
            if any(cond(ii-dt:ii+dt,k)) then
                cond(ii,k) = True
            end if
        end do
        end do
    end if

    cond2 := cond(:,0)
    if num_ilev.gt.1 then 
        do k = 1,num_ilev-1
            cond2 = cond2.and.cond(:,k)
        end do
    end if

    B1_vals := ind( cond2(tvals1) )+1

    printline()
    print("Box #1 first guess events:")
    print("  "+ecal(B1_vals,0)+"-"+ecal(B1_vals,1)+"-"+ecal(B1_vals,2) )
    print("")
    
    ;--------------------------------------
    ; Identify potential arrival dates in Box 2
    ;--------------------------------------
    min_arrival = B_distance / max_speed /86400.
    max_arrival = B_distance / min_speed /86400.
    min_arr = toint( floor(min_arrival) )
    max_arr = toint( ceil (max_arrival) )

    B2_vals_min := B1_vals + min_arr
    B2_vals_max := B1_vals + max_arr


    ; do i = 0,dimsizes(B1_vals)-1
    ;     print("D   "+ecal(B1_vals(i),0)+"-"+ecal(B1_vals(i),1)+"-"+ecal(B1_vals(i),2) )
    ;     print("A1       "+ecal(B2_vals_min(i),1)+"-"+ecal(B2_vals_min(i),2) )
    ;     print("A2       "+ecal(B2_vals_max(i),1)+"-"+ecal(B2_vals_max(i),2) )
    ;     print("")
    ; end do

    ;--------------------------------------
    ; Look for events in arrival box (#2) 
    ;--------------------------------------
    ; check main index level for events
    cond := new((/ntime,num_ilev/),logical)
    cond(tvals1,:) = E2(tvals1,:).gt.0. .and. \
                     E2(tvals1,:).ge.E2(tvals0,:) .and. \
                     E2(tvals1,:).ge.E2(tvals2,:)
    if .not.any(cond) then printExit("No events found in Box 2") end if
    
    ; Check other levels to make sure event is sufficiently deep
    ; a time delay of dt is allowed for the peak EVR
    if num_ilev.gt.1 then 
        B2_vals := ind( cond(:,0) );+1
        do i = 0,dimsizes(B2_vals)-1
        do k = 1,num_ilev-1
            dt = 1
            ii = B2_vals(i)
            if any(cond(ii-dt:ii+dt,k)) then
                cond(ii,k) = True
            end if
        end do
        end do
    end if

    ; screen events at levels that don't match with main index level
    cond2 := cond(:,0)
    if num_ilev.gt.1 then 
        do k = 1,num_ilev-1
            cond2 = cond2.and.cond(:,k)
        end do
    end if

    ;--------------------------------------
    ; find associated departure dates
    ;--------------------------------------
    B2_A_ind := ind( cond2(tvals1) )+1               ; Arrival index
    B2_D_ind := new(dimsizes(B2_A_ind),integer)      ; Departure index

    do i = 0,dimsizes(B2_A_ind)-1
        condition := B2_A_ind(i).ge.B2_vals_min .and. B2_A_ind(i).le.B2_vals_max
        if .not.any( condition ) then 
            ; B2_A_ind(i) = -1
        else
            t_ind := ind(condition)
            ; B2_D_ind(i) = min(t_ind)      ; select fastest wave
            ; B2_D_ind(i) = avg(t_ind)      ; select average wave
            B2_D_ind(i) = max(t_ind)      ; select slowest wave
        end if
    end do

    ;--------------------------------------
    ; identify duplicate departure dates
    ;--------------------------------------
    printline()
    print("Duplicate departure dates:")
    do i = 0,dimsizes(B2_D_ind)-1
        dup_cond := B2_D_ind.eq.B2_D_ind(i)
        if num(dup_cond).gt.1 then
            print("  "+ecal(B2_D_ind(i),0)+"-"+ecal(B2_D_ind(i),1)+"-"+ecal(B2_D_ind(i),2) )
            dup_ind := ind(dup_cond)
            ndup = dimsizes(dup_ind)
            B2_D_ind(dup_ind(0:ndup-2)) = B2_D_ind@_FillValue   ; keep slowest wave
            ; B2_D_ind(dup_ind(1:ndup-1)) = B2_D_ind@_FillValue   ; keep fastest wave
        end if
    end do
    print("")

    ;--------------------------------------
    ; print arrivial events
    ;--------------------------------------
    printline()
    print("Box #2 first guess events:")
    do i = 0,dimsizes(B2_A_ind)-1
    if B2_A_ind(i).ne.-1 then 
        print("  "+ecal(B2_A_ind(i),0)+"-"+ecal(B2_A_ind(i),1)+"-"+ecal(B2_A_ind(i),2) )
    end if
    end do
    print("")

    ;-----------------------------------------------------------------------------
    ;-----------------------------------------------------------------------------
    ; Plot EVR hovmoller with markers for verifications
    ;-----------------------------------------------------------------------------
    ;-----------------------------------------------------------------------------
    if plot_hov then 
        printline()
        tfig_file = fig_file+".hov"
        wks = gsn_open_wks(fig_type,tfig_file)
        plot = new(num_ilev,graphic)
            res = setres_contour_fill()
            res@vpWidthF    = 0.4
            res@cnLevelSelectionMode = "ExplicitLevels"
            res@cnLevels = ispan(-25,25,5)

            mres = setres_default()
            mres@xyMarkLineMode = "Markers"
            mres@xyMarkerColor  = "black"
            mres@xyMarker       = 16
            mres@xyMarkerSizeF  = 0.01

            lres = setres_default()
            lres@xyDashPattern      = 1
            lres@xyLineThicknessF   = 3.

        do k = 0,num_ilev-1
            tmp := dim_avg_n_Wrap( E(:,{ilev(k)},{10:25},:) ,1)
            ptime := tmp&time
            ptime = ( ptime - ptime(0) )/24.
            tmp&time = ptime
            res@gsnRightString = ilev(k)+"mb"
            plot(k) = gsn_csm_contour(wks,tmp,res)
            ;-------------------------------------
            ; Overlay marker for departure dates
            ;-------------------------------------
            xx = (/1,1/)
            x1 = ( B1_lon2 + B1_lon1 ) / 2.
            x2 = ( B2_lon2 + B2_lon1 ) / 2.
            do i = 0,dimsizes(B1_vals)-1
                if .not.ismissing(B1_vals(i)) then 
                    overlay(plot(k), gsn_csm_xy(wks,xx*x1,xx*ptime(B1_vals(i)),mres) )
                end if
            end do
            ;-------------------------------------
            ; Overlay marker for arrival dates
            ; and draw line to show propagation
            ;-------------------------------------
            do i = 0,dimsizes(B2_A_ind)-1
                if B2_A_ind(i).gt.(0) then
                    overlay(plot(k), gsn_csm_xy(wks,xx*x2,xx*ptime(B2_A_ind(i)),mres) )
                end if
                if .not.ismissing(B2_D_ind(i)) then 
                    ; tD_val = B2_D_ind(i)
                    tD_val = B1_vals( B2_D_ind(i) )
                    yy := ptime( (/tD_val,B2_A_ind(i)/) )
                    overlay(plot(k), gsn_csm_xy(wks,(/x1,x2/),yy,lres) )
                end if
            end do
        end do
        gsn_panel(wks,plot,(/1,dimsizes(plot)/),setres_panel_labeled())
        trimPNG(tfig_file)
    end if
    ;-----------------------------------------------------------------------------
    ;-----------------------------------------------------------------------------
    
    ;--------------------------------------
    ; Setup variables for tracing vortex path
    ;--------------------------------------
    vals := ind(.not.ismissing(B2_D_ind))
    event_D_ind = B2_D_ind(vals)
    ; vals := ind(.not.ismissing(B2_A_ind))
    ; event_A_ind = B2_A_ind(vals)
    num_event = dimsizes(event_D_ind)

    event_time = ispan(0,10,1)
    num_et = dimsizes(event_time)

    xdim = (/num_event,num_ilev,num_et/)
    vortex_lat = new(xdim,float)
    vortex_lon = new(xdim,float)

    ;--------------------------------------
    ; Trace approx propagation path
    ;--------------------------------------
printline()
    do i = 0,dimsizes(event_D_ind)-1
        t1 := event_D_ind(i)
        ; tE = E(t1,{ilev(0)},{B1_lat1:B1_lat2},{B1_lon1:B1_lon2})
        lost = False
        do t = 0,num_et-1
        do k = 0,0;num_ilev-1
            if t.eq.0 then 
                tlat1 := B1_lat1
                tlat2 := B1_lat2
                tlon1 := B1_lon1
                tlon2 := B1_lon2
            else
                ctr_lat = avg(vortex_lat(i,:,t-1))
                ctr_lon = avg(vortex_lon(i,:,t-1))
                ctr_rad = 10.
                tlat1 := ctr_lat - ctr_rad
                tlat2 := ctr_lat + ctr_rad
                tlon1 := ctr_lon - ctr_rad
                tlon2 := ctr_lon + ctr_rad
            end if

            if .not.lost then 
                
                tt = t1+t

                tE := E(tt,{ilev(k)},{tlat1:tlat2},{tlon1:tlon2})

; qplot_contour_fill(tE)

                maxloc := local_max(tE,False,0.1)

                ; vortex_lat(i,k,t) = elat( maxloc@yi )
                ; vortex_lon(i,k,t) = elon( maxloc@xi )

                if maxloc.gt.0 then
                    
                    ; max_ind = ind( maxloc@maxval .eq. max(maxloc@maxval) )
                    max_ind := ind( maxloc@xi .eq. max(maxloc@xi) )

                    if dimsizes(max_ind).gt.1 then
                        max_ind := ind( maxloc@maxval(max_ind) .eq. max(maxloc@maxval(max_ind)) )
                    end if

                    vortex_lat(i,k,t) = tE&lat( maxloc@yi(max_ind) )
                    vortex_lon(i,k,t) = tE&lon( maxloc@xi(max_ind) )

                    print(i+"    "+t+"    "+vortex_lat(i,k,t)+"       "+vortex_lon(i,k,t))
                else
                    print("Dude, where's my vortex?!")
                    lost = True
                end if

            end if
        end do
        end do
        print("")
    end do

    ;-----------------------------------------------------------------------------
    ; Plot map of vortex tracks
    ;-----------------------------------------------------------------------------
    if plot_map then
        tfig_file = fig_file+".map"
        wks := gsn_open_wks(fig_type,tfig_file)
            res := setres_default
            res@mpMinLatF = 0
            res@mpMaxLatF = 40
            res@mpMinLonF = -90
            res@mpMaxLonF = 0
        plot := gsn_csm_map(wks,res)

            lres = setres_default()
            lres@xyMarkLineMode = "MarkLines"
            lres@xyMarker           = 14
            lres@xyDashPattern      = 0
            lres@xyLineThicknessF   = 6.
            ; lres@xyLineColors       = (/"red","green","blue"/)
        
        ; do i = 0,dimsizes(event_D_ind)-1
        do i = 0,2
            if i.eq.0 then lres@xyLineColor = "red" end if
            if i.eq.1 then lres@xyLineColor = "green" end if
            if i.eq.2 then lres@xyLineColor = "blue" end if
            if i.eq.3 then lres@xyLineColor = "purple" end if
            if i.eq.4 then lres@xyLineColor = "orange" end if
            if i.eq.5 then lres@xyLineColor = "cyan" end if
            overlay(plot,gsn_csm_xy(wks,vortex_lon(i,0,:),vortex_lat(i,0,:),lres))
        end do

        draw(plot)
        frame(wks)
        trimPNG(tfig_file)
    end if
    ;-----------------------------------------------------------------------------
    ;-----------------------------------------------------------------------------

    ;--------------------------------------
    ;--------------------------------------

;==================================================================================
; Write wave event information to a file
;==================================================================================




;==================================================================================
;==================================================================================
end
