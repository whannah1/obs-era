; Plot a distribution of AEW events over some box
; v1 - basic version
; v2 - compare years with low/high genesis fraction - from James' spreadsheet
; v3 - compare years with low ocean event count
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
load "$NCARG_ROOT/custom_functions_ERAi.ncl"
begin

    case = (/"ERAi","ERAi","ERAi","ERAi"/)
    
    ;lev = 600
    
    fig_type = "png"
    fig_file = "~/Research/Obs/ERAi/figs_AEW/AEW.event.distribution.v2"
    
    normalize = False
;===================================================================================
;===================================================================================
    

    season = "JAS"

    ;case = where(case.ne."ERAi",case+"-CPL_0.9x1.25_gx1v6_01",case)
    ;casename = CESM_get_casename(case)
    if .not.isvar("casename") then casename = case end if
    num_c = dimsizes(case)

    opt  = True
    opt@debug = False
    ;opt@ERAIyr1   = 1995
    ;opt@ERAIyr2   = 2014
    ;opt@lat1  = ilat1
    ;opt@lat2  = ilat2
    ;opt@lon1  = ilon1
    ;opt@lon2  = ilon2
    ;opt@lev = lev

    genesis_frac = (/0.667,0.786,0.684,0.55,0.571,0.818,0.647,0.5,0.7,0.655,0.667,0.762,0.357,0.647,0.667,0.625,0.714,0.25,0.692,0.81/)
    genesis_frac = genesis_frac(::-1)
    gyear = ispan(1995,1995+dimsizes(genesis_frac)-1,1)
    genesis_frac!0 = "year"
    genesis_frac&year = gyear

    threshold = dim_median(genesis_frac)


    title = ""
    wks  = gsn_open_wks(fig_type,fig_file)
    gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
    plot = new(4,graphic)
    dum = new(dimsizes(plot),graphic)
        res = setres_xy()

    ;------------------------------------------------
    ; Create inset map
    ;------------------------------------------------
    mapplot = new(2,graphic)
    mopt = True
    mopt@lat1 = -10
    mopt@lat2 =  40
    mopt@lon1 = -40
    mopt@lon2 =  50
    mopt@mapx  = 0.3
    mopt@mapy  = 0.98
    mapplot(0) = plot_map_box(wks,mopt)
;===================================================================================
;===================================================================================
pclr = new(num_c,string)
do c = 0,num_c-1
    if any(c.eq.(/0,2/)) then vals := ind(genesis_frac.ge.threshold) end if
    if any(c.eq.(/1,3/)) then vals := ind(genesis_frac.lt.threshold) end if
    tyear := gyear(vals)
    nyear := dimsizes(tyear)

    if any(c.eq.(/0,1/)) then
        lev = 600
        opt@lev = lev
        ilat1 =   5
        ilat2 =  15
        ilon1 = -10    ;-15.
        ilon2 =   0    ;-5.;0.
        clr = "blue"
    end if
    if any(c.eq.(/2,3/)) then
        lev = 850
        opt@lev = lev
        ; Northern track
        ;ilat1 =  15.
        ;ilat2 =  25.
        ;ilon1 = -15. 
        ;ilon2 =   0. 
        ; over ocean MDR
        lev = 600
        ilat1 =   5
        ilat2 =  15
        ilon1 = -30
        ilon2 = -20
        clr = "red"
    end if
    pclr(c) = clr
    opt@lat1  = ilat1
    opt@lat2  = ilat2
    opt@lon1  = ilon1
    opt@lon2  = ilon2
    ;-----------------------------------------------
    ;-----------------------------------------------
    ;opt@monthly = True
    ;Uj := LoadERAi("U",opt) 
    ;mn := Uj@mn
    ;condition := mn.eq.7 .or. mn.eq.8 .or. mn.eq.9
    ;vals := where(condition,ispan(0,dimsizes(mn)-1,1),-1)
    ;if .not.all(ismissing(vals)) then
    ;    vals:= vals(ind(vals.ge.0))
    ;    Uj := Uj(vals,:,:)
    ;end if
    ;Uj@long_name    = lev+"mb Zonal Wind"
    ;-----------------------------------------------
    ;-----------------------------------------------    
    opt@monthly = False
    do y = 0,nyear-1
        opt@ERAIyr1   = tyear(y)
        opt@ERAIyr2   = tyear(y)
        iEKE := LoadERAi("EKE",opt) 
        mn := iEKE@mn
        condition := mn.eq.7 .or. mn.eq.8 .or. mn.eq.9
        vals := where(condition,ispan(0,dimsizes(mn)-1,1),-1)
        if .not.all(ismissing(vals)) then
            vals := vals(ind(vals.ge.0))
            iEKE := iEKE(vals,:,:)
        end if
        if y.eq.0 then 
            nt   = dimsizes(iEKE&time)
            nlat = dimsizes(iEKE&lat)
            nlon = dimsizes(iEKE&lon)
            EKE = new((/nt*nyear,nlat,nlon/),float)
            EKE@long_name = lev+"mb EKE "
            EKE@units = "m~S~2~N~ s~S~-2~N~"
        end if
        t1 = nt*y
        t2 = nt*(y+1)-1
        EKE(t1:t2,:,:) = iEKE
        delete(iEKE)
    end do
    nt   = dimsizes(EKE&time)
    nlat = dimsizes(EKE&lat)
    nlon = dimsizes(EKE&lon)
    ;-----------------------------------------------
    ; make event index
    ;-----------------------------------------------
    index := dim_avg_n_Wrap(EKE(:,{ilat1:ilat2},{ilon1:ilon2}),(/1,2/)) 

    printMAMS(index)
    
    cond := new(dimsizes(index),logical)
    ndim := dimsizes(dimsizes(index))
    if ndim.eq.1 then
        cond(1:nt-2) = (index(0:nt-3).lt.index(1:nt-2)) .and.\
                       (index(2:nt-1).lt.index(1:nt-2))
    end if
    if ndim.eq.3 then
        cond(1:nt-2,:,:) = (index(0:nt-3,:,:).lt.index(1:nt-2,:,:)) .and.\
                           (index(2:nt-1,:,:).lt.index(1:nt-2,:,:))
    end if
    ;condition := conform(EKE(1:nt-2,:,:),cond,(/0/))
    index = where(cond,index,index@_FillValue)

    tmp = ndtooned(index)
    index := tmp
    delete(tmp)
    ;-----------------------------------------------
    ; make distribution of index
    ;-----------------------------------------------
    bopt = True
    if normalize then
        index = index / stddev(index)
        bopt@bin_min = 0.25
        bopt@bin_max = 5.0
        bopt@bin_spc = 0.25
        res@tiXAxisString = "Event PKE Normalized by Std. Dev."
    else
        bopt@bin_min =  2
        bopt@bin_max = 30
        bopt@bin_spc =  4
        res@tiXAxisString = "Peak Event PKE [m~S~2~N~ s~S~-2~N~]"
    end if
    
    tdist := bin_cnt(index,bopt)
    if c.eq.0 then 
        xbin = tdist&bin
        nbin = dimsizes(xbin)
        dist = new((/num_c,nbin/),float)
    end if
    dist(c,:) = tdist / nyear
    
    ;comp := new((/3,nlat,nlon/),float)
    ;comp(0,:,:) = dim_avg_n( where(condition,EKE(1:nt-2,:,:),EKE@_FillValue) ,0)
    ;comp!0 = "lag"
    ;comp!1 = "lat"
    ;comp!2 = "lon"
    ;comp&lat = EKE&lat
    ;comp&lon = EKE&lon

    delete([/EKE/])

    ;-----------------------------------------------
    ; add box to inset map
    ;-----------------------------------------------
    mopt@blat1  = ilat1
    mopt@blat2  = ilat2
    mopt@blon1  = ilon1
    mopt@blon2  = ilon2
    mopt@bcolor = clr
    plot_add_box(wks,mapplot(0),mopt)

end do
;===================================================================================
;===================================================================================
        res@gsnLeftString = "ERAi AEW Event Distribution ("+season+")"
        res@tiYAxisString = "Average Count per Year"
        res@xyLineColors  = pclr
        res@xyDashPatterns = (/0,1,0,1,0,1/)

    plot(0) = gsn_csm_xy(wks,xbin,dist,res)

    ;-----------------------------------------------
    ; Overlay ?
    ;-----------------------------------------------
    
if False then 
    ;------------------------------------------------
    ; add inset map
    ;------------------------------------------------
    mopt = True
    mopt@lat1 = -10
    mopt@lat2 =  40
    mopt@lon1 = -40
    mopt@lon2 =  50
    ;mopt@blat1 = ilat1
    ;mopt@blat2 = ilat2
    ;mopt@blon1 = ilon1
    ;mopt@blon2 = ilon2
    mopt@mapx  = 0.3
    mopt@mapy  = 0.98
    ;mopt@mapx  = 0.7
    ;mopt@mapy  = 0.8
    mapplot = plot_map_box(wks,mopt)

    mopt@blat1 =   5.
    mopt@blat2 =  15.
    mopt@blon1 = -10.
    mopt@blon2 =  -5.
    mopt@bcolor = "blue"
    plot_add_box(wks,mapplot,mopt)

    ;mopt@blat1 = 15
    ;mopt@blat2 = 25
    ;mopt@blon1 = -15
    ;mopt@blon2 = -10

    mopt@blat1 =   5.
    mopt@blat2 =  15.
    mopt@blon1 = -25.
    mopt@blon2 = -20.
    mopt@bcolor = "red"
    plot_add_box(wks,mapplot,mopt)
    
    ;-----------------------------------------------
    ;-----------------------------------------------

end if
;===================================================================================
;===================================================================================
        pres = setres_panel();_labeled()
        pres@gsnFrame = False
        pres@gsnPanelFigureStringsFontHeightF = 0.01

    gsn_panel(wks,plot,(/2,2/),pres)


    legend = create "Legend" legendClass wks
        "lgAutoManage"             : False
        "vpXF"                     : 0.3
        "vpYF"                     : 0.8
        "vpWidthF"                 : 0.08
        "vpHeightF"                : 0.05
        "lgPerimOn"                : True
        "lgItemCount"              : 2
        "lgLabelStrings"           : (/"Low ","High "/)+"Genesis Frac"
        "lgLabelsOn"               : True
        "lgLineLabelsOn"           : False
        "lgLabelFontHeightF"       : 0.008
        "lgDashIndexes"            : (/1,0/)
        "lgLineThicknessF"         : 6.
        "lgLineColor"              : "black"
        "lgMonoLineColor"          : True
        "lgMonoDashIndex"          : False
    end create

    draw(mapplot)
    draw(legend)
    frame(wks)

    trimPNG(fig_file)
;===================================================================================
;===================================================================================
end
