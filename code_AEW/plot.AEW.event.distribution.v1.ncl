; Plot a distribution of AEW events over some box
; v1 - basic version
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
load "$NCARG_ROOT/custom_functions_ERAi.ncl"
begin

    case = (/"ERAi"/)
    
    lev = 600
    
    fig_type = "png"
    fig_file = "~/Research/Obs/ERAi/figs_AEW/AEW.event.distribution.v1."+lev
    
;===================================================================================
;===================================================================================
    if lev.eq.600 then
        ilat1 =  5. ;- 2.
        ilat2 = 15. ;- 2.
        ilon1 = -5.
        ilon2 =  0.
    end if

    if lev.eq.850 then
        ilat1 =   5. + 2.
        ilat2 =  15. + 2.
        ilon1 = -15. ;+ 360.
        ilon2 = -10. ;+ 360.
    end if

    season = "JAS"

    ;case = where(case.ne."ERAi",case+"-CPL_0.9x1.25_gx1v6_01",case)
    ;casename = CESM_get_casename(case)
    if .not.isvar("casename") then casename = case end if
    num_c = dimsizes(case)

    opt  = True
    opt@debug = False
    opt@ERAIyr1   = 1990
    opt@ERAIyr2   = 2014
    opt@lat1  = ilat1
    opt@lat2  = ilat2
    opt@lon1  = ilon1
    opt@lon2  = ilon2
    opt@lev = lev

    title = ""
    wks  = gsn_open_wks(fig_type,fig_file)
    gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
    plot = new(4,graphic)
    dum = new(dimsizes(plot),graphic)
        res = setres_xy()

;===================================================================================
;===================================================================================
do c = 0,num_c-1
    ;-----------------------------------------------
    ;-----------------------------------------------
if False
    opt@monthly = True
    Uj := LoadERAi("U",opt) 
    mn := Uj@mn
    ;condition := mn.eq.6 .or. mn.eq.7 .or. mn.eq.8 .or. mn.eq.9
    condition := mn.eq.7 .or. mn.eq.8 .or. mn.eq.9
    vals := where(condition,ispan(0,dimsizes(mn)-1,1),-1)
    if .not.all(ismissing(vals)) then
        vals:= vals(ind(vals.ge.0))
        Uj := Uj(vals,:,:)
    end if
    Uj@long_name    = lev+"mb Zonal Wind"
end if
    ;-----------------------------------------------
    ;-----------------------------------------------
    opt@monthly = False
    EKE := LoadERAi("EKE",opt) 
    mn := EKE@mn
    ;condition := mn.eq.6 .or. mn.eq.7 .or. mn.eq.8 .or. mn.eq.9
    condition := mn.eq.7 .or. mn.eq.8 .or. mn.eq.9
    vals := where(condition,ispan(0,dimsizes(mn)-1,1),-1)
    if .not.all(ismissing(vals)) then
        vals := vals(ind(vals.ge.0))
        EKE := EKE(vals,:,:)
    end if
    EKE@long_name  = lev+"mb EKE "
    EKE@units   = "m~S~2~N~ s~S~-2~N~"

    nt   = dimsizes(EKE&time)
    nlat = dimsizes(EKE&lat)
    nlon = dimsizes(EKE&lon)
    ;-----------------------------------------------
    ;-----------------------------------------------
    index := dim_avg_n_Wrap(EKE(:,{ilat1:ilat2},{ilon1:ilon2}),(/1,2/)) 

    printMAMS(index)
    
    ;isd = stddev(index)
    ;print("      std dev : "+isd)
    ;index = index / stddev(index)
    
    ;threshold = 1.5
    ;cond := (index(1:nt-2).gt.threshold)     .and.\
    ;        (index(0:nt-3).lt.index(1:nt-2)) .and.\
    ;        (index(2:nt-1).lt.index(1:nt-2))
    ;condition := conform(EKE(1:nt-2,:,:),cond,(/0/))            
    ;num_cond = num(cond)

    bopt = True
    if True then
        index = index / stddev(index)
        bopt@bin_min = 0.25
        bopt@bin_max = 5.0
        bopt@bin_spc = 0.25
        res@tiXAxisString = "Event PKE Normalized by Std. Dev."
    else
        bopt@bin_min =  0
        bopt@bin_max = 40
        bopt@bin_spc =  2
        res@tiXAxisString = "Peak Event PKE [m~S~2~N~ s~S~-2~N~]"
    end if
    
    tdist := bin_cnt(index,bopt)
    if c.eq.0 then 
        xbin = tdist&bin
        nbin = dimsizes(xbin)
        dist = new((/num_c,nbin/),float)
    end if
    dist(c,:) = tdist
    
    ;comp := new((/3,nlat,nlon/),float)
    ;comp(0,:,:) = dim_avg_n( where(condition,EKE(1:nt-2,:,:),EKE@_FillValue) ,0)
    ;comp!0 = "lag"
    ;comp!1 = "lat"
    ;comp!2 = "lon"
    ;comp&lat = EKE&lat
    ;comp&lon = EKE&lon
end do
;===================================================================================
;===================================================================================
        res@gsnLeftString = "AEW Event Distribution ("+season+")"
        res@tiYAxisString = "Count"
        ;res@xyLineColors  = CESM_get_color(case)
        res@xyDashPattern = 0

    plot(0) = gsn_csm_xy(wks,xbin,dist,res)

    ;-----------------------------------------------
    ; Overlay ?
    ;-----------------------------------------------
    
    ;------------------------------------------------
    ; add inset map
    ;------------------------------------------------
    mopt = True
    mopt@lat1 = -10
    mopt@lat2 =  40
    mopt@lon1 = -40
    mopt@lon2 =  50
    mopt@blat1 = ilat1
    mopt@blat2 = ilat2
    mopt@blon1 = ilon1
    mopt@blon2 = ilon2
    mopt@mapx  = 0.3
    mopt@mapy  = 0.98
    ;mopt@mapx  = 0.7
    ;mopt@mapy  = 0.8
    mapplot = plot_map_box(wks,mopt)
    draw(mapplot)
    ;-----------------------------------------------
    ;-----------------------------------------------


;===================================================================================
;===================================================================================
        pres = setres_panel();_labeled()
        pres@gsnPanelFigureStringsFontHeightF = 0.01

    gsn_panel(wks,plot,(/2,2/),pres)

    trimPNG(fig_file)
;===================================================================================
;===================================================================================
end
