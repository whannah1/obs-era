load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
load "$NCARG_ROOT/custom_functions_ERAi.ncl"
begin

    lat1 = -30
    lat2 =  40
    ;lon1 = -60
    ;lon2 =  60
    
    idir = "~/Data/Obs/ERAi/data/"

    var = "EKE"

    ;yr1 = 2014
    ;yr2 = yr1;+1
    ;yr2 = 2014     
    ; for some reason systemfunc() in LoadERAi() "cannot create child process" 
    ; on the third year, so I was forced to create these files 2 years at a time

;==================================================================================
;==================================================================================
    fca = 1./(10.*4)    ; longer period
    fcb = 1./( 2.*4)    ; shorter period
    opt = True
    opt@lat1 = lat1
    opt@lat2 = lat2
    ;opt@lon1 = lon1
    ;opt@lon2 = lon2

    nwgt  = 15*4+1
    sigma = 1.0
    wgt   = filwgts_lanczos (nwgt,2,fca,fcb,sigma)
;==================================================================================
; Loop through years
;==================================================================================
num_y = yr2-yr1+1
do y = 0,num_y-1
    ofile = idir+"ERAi.6hour.1.0-deg."+var+"."+(yr1+y)+".no_leap.nc"
    ;-----------------------------------------------------------
    ; Load data
    ;-----------------------------------------------------------
    ;print("loading U")
    ;infile = addfile(idir+"ERAi.6hour.1.0-deg.U."+(yr1+y)+".no_leap.nc","r")
    ;U = infile->U(:,:,{lat1:lat2},{lon1:lon2})
    ;print("loading V")
    ;infile = addfile(idir+"ERAi.6hour.1.0-deg.V."+(yr1+y)+".no_leap.nc","r")
    ;V = infile->V(:,:,{lat1:lat2},{lon1:lon2}) 

    opt@ERAIyr1 = yr1+y ;-1
    opt@ERAIyr2 = yr1+y ;+1

    U := LoadERAi("U",opt)
    V := LoadERAi("V",opt)
    ;-----------------------------------------------------------
    ; Coordinates
    ;-----------------------------------------------------------
    time = U&time
    lev = U&lev
    lat = U&lat
    lon = U&lon

    lev!0   = "lev"
    lat!0   = "lat"
    lon!0   = "lon"
    lev&lev = lev
    lat&lat = lat
    lon&lon = lon
    lat@units = "degrees north"
    lon@units = "degrees east"

    levsz  = dimsizes(lev)
    latsz  = dimsizes(lat)
    lonsz  = dimsizes(lon)
    ;-----------------------------------------------------------
    ; Vertically integrate
    ;-----------------------------------------------------------
    ;Up := bw_bandpass_filter (U,fca,fcb,opt,0) 
    ;Vp := bw_bandpass_filter (V,fca,fcb,opt,0) 
    Up := wgt_runave_n_Wrap(U,wgt,0,0)
    Vp := wgt_runave_n_Wrap(V,wgt,0,0)
    
    EKE := ( Up^2. + Vp^2. )/2.

    copy_VarCoords(U,EKE)
    time := EKE&time
    time@calendar = "noleap"
    date  := cd_calendar(time,0)
    EKE@yr := date(:,0)
    EKE@mn := date(:,1)
    ;-----------------------------------------------------------
    ; Write output file
    ;-----------------------------------------------------------
    if fileexists(ofile) then system("rm "+ofile) end if
    ;print("status  : "+fileexists(ofile))
    print("writing to file...  "+ofile+"")
    outfile = addfile(ofile,"c")
    outfile->$var$ = EKE
    ;-----------------------------------------------------------
    ;-----------------------------------------------------------
    delete([/U,V,lev,lat,lon/])
end do
;==================================================================================
;==================================================================================

end
