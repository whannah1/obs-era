; Calculate the ccolumn integrated budget of either MSE / DSE / Lv*qv
; data is first interpolated to pressure levels from raw data
; to be consistent with the way obsevational budgets are calculated
; 6-hourly data is preferred, as daily data 
; results in a much larger residual
; v1 - basic version, does not calculate eddy flux terms, 
; each variable and year is written to separate file
; - also needs parent script - see code_data/mk.budget.bulk.ncl 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
load "$NCARG_ROOT/custom_functions_ERAi.ncl"
begin

    case  = "ERAi"
    odir = "~/Data/Obs/ERAi/data/"
    
    var  = "DSE" 

    debug = False

    yr1 = 2000
    yr2 = 2000
;====================================================================================================
;====================================================================================================
    dt = 6*3600.

    ofile_stub = odir+case+".6hour.1.0-deg." 
    num_c = dimsizes(case)

    ; specify variables used for long names (i.e. UdYdxvi)
    if var.eq."MSE" then var2 = "H" end if
    if var.eq."DSE" then var2 = "S" end if
    if var.eq."LQ"  then var2 = "Q" end if

    if any(var.eq.(/"MSE","DSE","LQ"/)) then 
        unit0 = "J kg^-1"        ; non-vertically integrated variable
        unit1 = "J m^-2"         ; vertically integrated variable
        unit2 = "J kg^-1 s^-1"   ; non-vertically integrated tendency
        unit3 = "W m^-2"         ; vertically integrated tendency
    end if

    opt  = True
    opt@debug    = debug
    opt@lat1  =  -30.
    opt@lat2  =   30.
    ;opt@lon1  = -180.
    ;opt@lon2  =  180.
    opt@lev   = -1
    lev = ERAI_default_lev

    ; North Africa coords
    ; opt@lat1  =   0.
    ; opt@lat2  =  40.
    ; opt@lon1  = -20.
    ; opt@lon2  =  30.
;====================================================================================================
;====================================================================================================2
c = 0
do y = yr1,yr2
    opt@ERAIyr1  = y
    opt@ERAIyr2  = y
    ;--------------------------------------
    ; Create output file
    ;--------------------------------------
    ;ofile = odir+ofile_stub+"."+y+".nc"
    ofile_tail = "."+y+".no_leap.nc"
    ;print(" writing file >  "+ofile)
    ;if fileexists(ofile) then system("rm "+ofile) end if
    ;outfile = addfile(ofile,"c")
    ;--------------------------------------
    ; coordinate variables
    ;--------------------------------------
    lat = LoadERAilat(opt)
    lon = LoadERAilon(opt)
    printline()
    fmt = "%6.4g"
    print("  case: "+case)
    print("    lat = "+sprintf(fmt,min(lat))+" : "+sprintf(fmt,max(lat)))
    print("    lon = "+sprintf(fmt,min(lon))+" : "+sprintf(fmt,max(lon)))
    ;print("    lev = "+sprintf(fmt,max(lev))+" : "+sprintf(fmt,min(lev)))
    printline()
    ;--------------------------------------
    ; Load main budget variable
    ;--------------------------------------
    if any(var.eq.(/"MSE","DSE"/)) then T  = LoadERAi("T"  ,opt) end if
    if any(var.eq.(/"MSE","DSE"/)) then Z  = LoadERAi("GEO",opt) end if
    if any(var.eq.(/"MSE", "LQ"/)) then Q  = LoadERAi("Q"  ,opt) end if

    if var.eq."MSE" then S  =  T *cpd + Z + Q *Lv end if
    if var.eq."DSE" then S  =  T *cpd + Z end if
    if var.eq."LQ"  then S  =  Q *Lv end if
    

    if any(var.eq.(/"MSE","DSE"/)) then copy_VarCoords(T,S) end if 
    if any(var.eq.(/"MSE", "LQ"/)) then copy_VarCoords(Q,S) end if
    
    S@units = unit0

    if isvar("T") then delete(T) end if
    if isvar("Z") then delete(Z) end if
    if isvar("Q") then delete(Q) end if
    ;--------------------------------------
    ; Build dp for vertical interpolation
    ;--------------------------------------
    Ps = LoadERAi("PS",opt)
    P   = conform(S,lev,1)*100.
    Po  = 100000.
    P!1 = "lev"
    P&lev = lev
    P@units = "Pa"
    dP = calc_dP(P,Ps)
    delete(Ps)
    ;-----------------------------------------------------------   
    ; Local Eulerian tendency
    ;-----------------------------------------------------------   
    Svi    = dim_sum_n(S*dP/g,1)
    DSDT   = calc_ddt(S,dt)
    DSDTvi = dim_sum_n(DSDT*dP/g,1)
    COLDP  = dim_sum_n(dP/g,1)

    Svi!0 = "time"
    Svi!1 = "lat"
    Svi!2 = "lon"
    Svi&time = S&time
    Svi&lat  = S&lat
    Svi&lon  = S&lon
    copy_VarCoords(Svi,DSDTvi)
    copy_VarCoords(Svi,COLDP)
    copy_VarCoords(S,DSDT)
    Svi@long_name     = "Column Integrated "+var
    COLDP@long_name   = "Column Mass"
    DSDT  @long_name  = var+" Tendency"
    DSDTvi@long_name  = "Column "+var+" Tendency"
    Svi@units         = unit1
    COLDP@units       = "kg m^-2"
    DSDT  @units      = unit2
    DSDTvi@units      = unit3

    tvar = var
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = S

    tvar = var+"vi"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = Svi

    tvar = "COLDP"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = COLDP

    tvar = "d"+var2+"dt"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = DSDT

    tvar = "d"+var2+"dtvi"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = DSDTvi
    
    delete([/COLDP,DSDT,DSDTvi/])
    ;-----------------------------------------------------------
    ; Zonal advective tendency
    ;-----------------------------------------------------------
    U  = LoadERAi("U",opt)
    copy_VarCoords(S,U)
    UdSdx   = U*calc_ddx(S)
    UdSdxvi = dim_sum_n( UdSdx*dP/g,1)
    delete([/U/])

    copy_VarCoords(S  ,UdSdx  )
    copy_VarCoords(Svi,UdSdxvi)
    UdSdx  @long_name = "Adv "+var+" tendency (u*d_dx)"
    UdSdxvi@long_name = "Column Adv "+var+" tendency (u*d_dx)"
    UdSdx  @units = unit2
    UdSdxvi@units = unit3

    tvar = "Ud"+var2+"dx"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = UdSdx
    delete([/UdSdx/])

    tvar = "Ud"+var2+"dxvi"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = UdSdxvi
    delete([/UdSdxvi/])

    ;-----------------------------------------------------------
    ; Meridional advective tendency
    ;-----------------------------------------------------------
    V  = LoadERAi("V",opt)
    copy_VarCoords(S,V)
    VdSdy   = V*calc_ddy(S)
    VdSdyvi = dim_sum_n( VdSdy*dP/g,1)
    delete([/V/])

    copy_VarCoords(S  ,VdSdy  )
    copy_VarCoords(Svi,VdSdyvi)
    VdSdy  @long_name = "Adv "+var+" tendency (v*d_dy)"
    VdSdyvi@long_name = "Column Adv "+var+" tendency (v*d_dy)"
    VdSdy  @units = unit2
    VdSdyvi@units = unit3

    tvar = "Vd"+var2+"dy"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = VdSdy
    delete([/VdSdy/])

    tvar = "Vd"+var2+"dyvi"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = VdSdyvi
    delete([/VdSdyvi/])
    ;-----------------------------------------------------------
    ; Vertical advective tendency
    ;-----------------------------------------------------------
    W  = LoadERAi("OMEGA",opt)
    WdSdp   = W*calc_ddp(S)
    WdSdpvi = dim_sum_n( WdSdp*dP/g,1)
    delete([/W/])

    copy_VarCoords(S  ,WdSdp  )
    copy_VarCoords(Svi,WdSdpvi)
    WdSdp  @long_name = "Vert Adv "+var+" tendency (w*d_dp)"
    WdSdpvi@long_name = "Column Vert Adv "+var+" tendency (w*d_dp)"
    WdSdp  @units = unit2
    WdSdpvi@units = unit3


    tvar = "Wd"+var2+"dp"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = WdSdp
    delete([/WdSdp/])

    tvar = "Wd"+var2+"dpvi"
    outfile = createfile(ofile_stub+ tvar +ofile_tail)
    outfile->$tvar$ = WdSdpvi
    delete([/WdSdpvi/])
    ;-----------------------------------------------------------
    ;-----------------------------------------------------------
    delete([/dP,Svi,S/])
    ;***********************************************************************************
    ;***********************************************************************************
end do
;====================================================================================================
;====================================================================================================
end

