#!/usr/bin/env python3
import os, glob
import subprocess as sp
#---------------------------------------------------------------------------------------------------
# command to create map file:
'''
SRC_GRID=${HOME}/HICCUP/files_grid/scrip_721x1440_n2s.nc
DST_GRID=${HOME}/E3SM/data_grid/ne120pg2_scrip.nc
MAP_FILE=${HOME}/maps/map_721x1440_to_ne120pg2.nc
ncremap -a fv2fv --src_grd=$SRC_GRID --dst_grd=$DST_GRID --map_file=$MAP_FILE
ncks --chk_map $MAP_FILE
'''
#---------------------------------------------------------------------------------------------------
class clr:END,RED,GREEN,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[35m','\033[36m'
def run_cmd(cmd): 
    print('\n'+clr.GREEN+cmd+clr.END)
    os.system(cmd)
    return
#---------------------------------------------------------------------------------------------------

# scratch_root = '/global/cscratch1/sd/whannah/Obs/ERA5'            # NERSC
scratch_root = '/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5'     # OLCF

dst_grid = 'ne120pg2'
data_freq = 'daily'

overwrite = True

map_file = os.getenv('HOME')+f'/maps/map_721x1440_to_ne120pg2.nc'

#---------------------------------------------------------------------------------------------------

comp_list = []
# comp_list.append('sfc')
comp_list.append('atm')

idir = f'{scratch_root}/{data_freq}'
odir = f'{scratch_root}/{data_freq}_{dst_grid}'

for comp in comp_list:

    # files = glob.glob(f'{idir}/ERA5.daily.{comp}.*nc')
    files = glob.glob(f'{idir}/ERA5.daily.{comp}.*vertical_velocity.nc')
    
    if files==[]: print('ERROR: file list is empty?')

    for src_file_name in files:
        if 'unpacked' not in src_file_name:

            dst_file_name = src_file_name
            dst_file_name = dst_file_name.replace(idir,odir)
            dst_file_name = dst_file_name.replace('.nc',f'.remap_{dst_grid}.nc')

            src_file_name_unpck = src_file_name.replace('.nc',f'.unpacked.nc')

            # unpack the data
            if not os.path.isfile(src_file_name_unpck):
                run_cmd(f'ncpdq -5 --ovr -U {src_file_name} {src_file_name_unpck}')

            if overwrite and os.path.isfile(dst_file_name): os.remove(dst_file_name)

            # remap the data
            if not os.path.isfile(dst_file_name):
                run_cmd(f'ncremap -m {map_file} -i {src_file_name_unpck} -o {dst_file_name}  ')

                # not sure why I need this extra unpacking step...?
                run_cmd(f'ncpdq -5 --ovr -U {dst_file_name} {dst_file_name}')

            # run_cmd(f'ncrename -v lat,latitude -v lon,longitude {dst_file_name} ')
            # --------------------------------------------------------------------------
            print(f'\n\nsrc file: {src_file_name_unpck}\ndst file: {dst_file_name}\n')

#---------------------------------------------------------------------------------------------------
