#!/usr/bin/env python3
import os, glob
import subprocess as sp

# scratch_root = '/global/cscratch1/sd/whannah/Obs/ERA5'            # NERSC
scratch_root = '/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5'     # OLCF

yr_list = [f'{i:04d}' for i in range(1950,2014+1)]
# yr_list = [f'{i:04d}' for i in range(1960,1969+1)]
# yr_list = [f'{i:04d}' for i in range(2000,2019+1)]

# src_grid = '721x1440'
dst_grid = 'ne30pg2'


def main( regrid_data=True, create_map=False, 
          unpack=True, clean=True, 
          data_freq='monthly'):

    # if dst_grid=='ne30pg2': map_file = os.getenv('HOME')+f'/maps/map_721x1440_to_ne30pg2_aave_20210107.nc'
    if dst_grid=='ne30pg2': map_file = os.getenv('HOME')+f'/maps/map_721x1440_to_ne30pg2.nc'
    

    # --------------------------------------------------------------------------
    for yr in yr_list:

        if data_freq=='yearly':  num_f = 1
        if data_freq=='monthly': num_f = 12

        comp = []
        comp.append('sfc')
        # comp.append('atm')

        for f in range(1,num_f+1):

            for c in comp:

                src_file_name = f'{scratch_root}/{data_freq}/ERA5.{data_freq}.{c}.{yr}-{f:02d}.nc'

                src_file_name_unpck = src_file_name.replace('.nc',f'.unpacked.nc')

                # dst_file_name = src_file_name.replace('.nc',f'.remap_{dst_grid}.nc')
                dst_file_name = f'{scratch_root}/{data_freq}_{dst_grid}/ERA5.{data_freq}.{c}.{yr}-{f:02d}.nc'

                # unpack the data
                if not os.path.isfile(src_file_name_unpck):
                    run_cmd(f'ncpdq --ovr -U {src_file_name} {src_file_name_unpck}')

                # remap the data
                if not os.path.isfile(dst_file_name):
                    run_cmd(f'ncremap -m {map_file} -i {src_file_name_unpck} -o {dst_file_name}  ')

                # run_cmd(f'ncrename -v lat,latitude -v lon,longitude {dst_file_name} ')
                # --------------------------------------------------------------------------
                print(f'\n\nsrc file: {src_file_name_unpck}\ndst file: {dst_file_name}\n')

# --------------------------------------------------------------------------------------------------
class tcolor:
    """ simple class for coloring terminal text """
    ENDC, BLACK, RED     = '\033[0m','\033[30m','\033[31m'
    GREEN, YELLOW, BLUE  = '\033[32m','\033[33m','\033[34m'
    MAGENTA, CYAN, WHITE = '\033[35m','\033[36m','\033[37m'
# --------------------------------------------------------------------------------------------------
def run_cmd(cmd,prepend_line=True,use_color=True,shell=True,execute=True):
    """
    Common method for printing and running commands
    """
    prefix,suffix = '  ',''
    if prepend_line : prefix = '\n'+prefix
    msg = f'{prefix}{cmd}{suffix}'
    if use_color : msg = tcolor.GREEN + msg + tcolor.ENDC
    print(msg)
    if not execute : return
    if     shell: sp.check_call(cmd,shell=True)
    if not shell: sp.check_call(cmd.split())
    return
# --------------------------------------------------------------------------------------------------
if __name__ == '__main__': 
   # # Parse the command line options
   # parser = OptionParser()
   # parser.add_option('-i',dest='ifile',default=None,help='input file name')
   # (opts, args) = parser.parse_args()

   main()