#!/usr/bin/env python
#===========================================================================================
# This script retrieves ECMWF Interim ReAnalysis (ERAi) data. Files are put in the 
# ./raw_grib/ folder. Each year of data will consist of 4 files, one for each time of day
# available. The ncl script mk.yearly_files.from_grib.ncl will combine data into daily
# means and write out netCDF files.
#
#    Mar, 2013	Walter Hannah 		Colorado State University
#===========================================================================================
from ecmwfapi import ECMWFDataServer
import sys
import numpy as np
#===========================================================================================
# Setup ECMWF connection
#===========================================================================================
server = ECMWFDataServer()
#===========================================================================================
# Initialize variables
#===========================================================================================
#---------------------------------------------
# start and end years
#---------------------------------------------
start_year = 2014
end_year   = 2014

date1 = "0901"
date2 = "1231"

#odir = "/data2/whannah/OBS/ERAi/raw_grib/"
odir = "~/Data/OBS/ERAi/raw_grib/"

#---------------------------------------------
# time of day (Zulu)
#---------------------------------------------
times = ["00","06","12","18"]
#---------------------------------------------
# Specify which variabels to fetch
#---------------------------------------------
#var = ["U","V","OMEGA","T","Q","GEO","Ps","CWV"]
var = ["Ps","CWV","U","V","Q"]
#var = ["CWV"]
#===========================================================================================	
# Loop through each variable
#===========================================================================================
for v in xrange(len(var)):
	
	if var[v] == "GEO"   : par = ["129.128","pl"]	# Geopotential
	if var[v] == "T"     : par = ["130.128","pl"]	# Temperature
	if var[v] == "U"     : par = ["131.128","pl"]	# U-Wind
	if var[v] == "V"     : par = ["132.128","pl"]	# V-Wind
	if var[v] == "Q"     : par = ["133.128","pl"]	# Specific Humidity
	if var[v] == "OMEGA" : par = ["135.128","pl"]	# Vertical Pressure Velocity
	if var[v] == "DIV"   : par = ["154.128","pl"]	# Divergence
	if var[v] == "Ps"    : par = ["134.128","sfc"]	# Surface Pressure
	if var[v] == "CWV"   : par = ["137.128","sfc"]	# Column Water Vapor
	
	print "" 
	print "  ----------------------------------"
	print "   ----------------------------------"
	print "    var    => ",var[v]
	print "    par    => ",par[0]
	print "  ----------------------------------"
	print "  ----------------------------------"
	
	for y in xrange(start_year,end_year+1):
		for t in xrange(len(times)):
			#date   = str(y)+"0101/to/"+str(y)+"1231"
			date   = str(y)+date1+"/to/"+str(y)+date2
			time   = times[t]
			domain = "40/-180/-40/180"
			target = odir+"ERAi.0.25-deg."+var[v]+"."+time+"."+str(y)+".grib"
			lev = "70/100/125/150/175/200/250/300/400/500/600/650/700/750/800/825/850/875/900/925/950/975"	
			#-------------------------------------------------
			#-------------------------------------------------
			print ""
			print "     time   => ",time
			print "     date   => ",date
			print "     domain => ",domain
			print "     target => ",target
			print ""
			#-------------------------------------------------
			#-------------------------------------------------
			server.retrieve({
			  'grid'      : ".25/.25",
			  'dataset'   : "interim",
			  'stream'    : "oper",
			  'step'      :  0,
			  'param'     : par[0],
			  'levtype'   : par[1], 
			  'levelist'  : lev,
			  'area'      : domain,
			  'time'      : time,
			  'date'      : date,
			  'target'    : target, 
			  })
#===========================================================================================
#===========================================================================================
