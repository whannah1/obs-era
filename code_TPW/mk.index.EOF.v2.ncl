; uses normalized TPW instead of raw TPW
; !!! currently identical to v1 !!!
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    idir = "~/Data/OBS/ERAi/data/"

    ofile = "~/Data/OBS/ERAi/index.EOF.v2.nc"
    
    debug = False

    lat1 =  -20.
    lat2 =   20.
    lon1 =   40.
    lon2 =  180.

    bin_min = 0.1
    bin_max = 1.8
    bin_spc = 0.02
    
    num_h = 3           ; # seasonal cycle harmonics to remove
;====================================================================================================
; Load ERAi data
;====================================================================================================
    ifile  = systemfunc("ls -1 "+idir+"/*.1.0-deg."+"CWV"+"*.no_leap.nc")
    infile = addfiles(ifile,"r")
    TPW  = block_avg( infile[:]->CWV(:,{lat1:lat2},{lon1:lon2}) ,4)
    
    time  = infile[:]->time
    time := time(::4)
    ctime = cd_calendar(time,0)
    yr1 = min(ctime(:,0))
    yr2 = max(ctime(:,0))
    time@units = "hours since "+yr1+"-01-01 00:00"
    
    num_t = dimsizes(time)
    
    printline()
    printMAM(TPW)
    ;-----------------------------------------------------
    ; define critical TPW and wwc
    ;-----------------------------------------------------
    ifile := systemfunc("ls -1 "+idir+"/*.1.0-deg."+"CT"+"*no_leap.nc")
    infile = addfiles(ifile,"r")

    CT  = block_avg( infile[:]->CT(:,{lat1:lat2},{lon1:lon2}) ,4)

    m  = 2.2
    bT = (/268.,269.,270.,271.,272.,273.,274/)
    bw = (/56.2,58.6,60.9,63.1,66.4,68.2,70.1/)
    b  = avg( bw - m*bT )
    wc = m*CT + b

    TPW := TPW / wc

    copy_VarCoords(CT,TPW)

    TPW@long_name = "Normalized TPW"

    printline()
    printMAM(TPW)
    printline()
;====================================================================================================
; Remove the seasonal cycle
;====================================================================================================   
    sz = dimsizes(TPW)
    scycle = new((/365,sz(1),sz(2)/),float)
    do d = 0,364 scycle(d,:,:) = dim_avg_n(TPW(d::365,:,:),0) end do
    
    H = new(dimsizes(scycle),float)
    H = 0.
    do h = 0,num_h-1
        H = H + calcHarmonic(scycle,h)
    end do
    
    vals = ispan(0,num_t-1,1)%365
    
    TPW = (/ TPW - H(vals,:,:) /)
    
    delete(vals)
;====================================================================================================
; Calculate daily distribution
;====================================================================================================
    bins = True
    bins@bin_min = bin_min
    bins@bin_max = bin_max
    bins@bin_spc = bin_spc
    bins@verbose = False
    
    print("  binning data...")

    num_v = 1
    do v = 0,num_v-1
    do t = 0,num_t-1
        if v.eq.0 then Vy := TPW(t,:,:) end if
            
        if v.eq.0 then Vy := TPW(t,:,{ 60:90}) end if
        if v.eq.1 then Vy := TPW(t,:,{110:140}) end if
        
        Vx = Vy
        
        tmp = bin_YbyX(Vy,Vx,bins)

        if v.eq.0.and.t.eq.0 then 
            xbin = tmp&bin
            num_b = dimsizes(xbin)
            bdim = (/num_v,num_t,num_b/)
            dist = new(bdim,float)
        end if

        dist(v,t,:) = tmp@pct 
        delete([/Vy,Vx,tmp/])
    end do
    end do
    print("done.")
    dist!0 = "var"
    dist!1 = "time"
    dist!2 = "bin"
    dist&bin = xbin
;====================================================================================================
; Calculate EOFs
;====================================================================================================
    neof     = 3
    opt      = True
    opt@jopt = 0
    EOF  = new((/num_v,num_b/),float) 
    PC   = new((/num_v,num_t/),float) 
    eval  = new((/num_v/),float) 
    pcvar = new((/num_v/),float) 
    do v = 0,num_v-1
        print("v: "+v)
        teof = eofunc   (dist(var|v,bin|:,time|:) ,neof,opt) 
        tpc  = eofunc_ts(dist(var|v,bin|:,time|:),EOF,False)
printMAM(tpc)
exit
        EOF(v,:)  = teof(0,:)
        PC (v,:)  = tpc (0,:)
        eval(v)   = teof@eval(0)
        pcvar(v)  = teof@pcvar(0)
    end do

    t2 = dimsizes(PC(0,:))-1
    EOF     = (/ EOF * conform(EOF, sqrt(eval) ,0) /)
    
    print("Variance Explained:")
    print("          "+pcvar)
    printline()

    AMP = sqrt ( PC(0,:)^2. + PC(1,:)^2. )
    PHS = atan2( PC(1,:) , PC(0,:) )
;====================================================================================================
;====================================================================================================
    mode = (/1,2/)
    mode!0 = "mode"
    mode&mode = mode
    
    EOF!0 = "mode"
    EOF!1 = "bin"
    EOF&mode = mode
    EOF&bin  = xbin

    PC!0 = "mode"
    PC!1 = "time"
    PC&mode = mode
    PC&time = time
    
    AMP!0 = "time"
    AMP&time = time
    
    copy_VarCoords(AMP,PHS)
;====================================================================================================
; WRite to file
;====================================================================================================
    if isfilepresent(ofile) then system("rm "+ofile) end if
    outfile = addfile(ofile,"c")

    outfile->EOF   = EOF
    outfile->PC    = PC 
    outfile->AMP   = AMP
    outfile->PHS   = PHS
    outfile->bin   = xbin

;====================================================================================================
;====================================================================================================
end
