load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	idir = "/data2/whannah/OBS/ERAi/data/"

	fig_type = "png"
	fig_file = "~/Research/OBS/ERAi/figs_TPW/PA.timeseries.v1"
	
	debug = False

	lat1 =  -20.
	lat2 =   20.
	lon1 =   50.
	lon2 =  180.
	
	;lat1 =  -5.
	;lat2 =   5.
	;lon1 =   40.
	;lon2 =  90.
	
	yr1 = 2011
	yr2 = 2012
	
	threshold = 50.
	
;====================================================================================================
;====================================================================================================
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(3,graphic)
	num_p = dimsizes(plot)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.1
		res@tmXTOn                          = False
		res@tmYLMinorOn                     = False
		res@tmXBMinorOn                     = False
		res@gsnLeftStringFontHeightF        = 0.01
		res@gsnRightStringFontHeightF       = 0.01
		res@tmXBLabelFontHeightF            = 0.005
		;res@tmYLLabelFontHeightF            = 0.01
		res@tmXBLabelAngleF					= -60.
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnRightString                  = ""
		
		lres = res
		lres@xyLineColor 		= "black"
		lres@xyDashPattern 		= 0
		lres@xyLineThicknessF	= 1.

		res@xyLineColor 		= "black"
		res@xyDashPattern 		= 0
		res@xyLineThicknessF	= 3.
;====================================================================================================
;====================================================================================================
	;-----------------------------------------------------
	; Load ERAi data
	;-----------------------------------------------------
	infile = addfiles("/data2/whannah/OBS/ERAi/data/ERAi.6hour.1.0-deg.budget.Q."+ispan(yr1,yr2,1)+".no_leap.nc","r")	
	TPW  = block_avg( infile[:]->TPW(:,{lat1:lat2},{lon1:lon2}) ,4)
	time = block_avg( infile[:]->time ,4)
	time = time-time(0) 
	;time@units = "hours since 1980-01-01 00:00"
	time@units = "hours since "+yr1+"-01-01 00:00"
	ytime = (/ time /(24.*365.) /)
	ytime = (/ ytime - ytime(0) + yr1 /)
	;-----------------------------------------------------
	; Create area index
	;-----------------------------------------------------
	aindex = tofloat( dim_num_n(TPW.gt.threshold,(/1,2/)) )
	aindex = aindex - avg(aindex)
	aindex = aindex/stddev(aindex)	
	aindex!0    = "time"
	aindex&time = TPW&time
	;-----------------------------------------------------
	; Create plateau "intensity" index
	;-----------------------------------------------------
	qindex = tofloat( dim_avg_n(where(TPW.gt.threshold,TPW,TPW@_FillValue),(/1,2/)) )
	qindex = qindex - avg(qindex)
	qindex = qindex/stddev(qindex)	
	qindex!0    = "time"
	qindex&time = TPW&time
	;-----------------------------------------------------
	; Calculate Amplitude and phase of new index
	;-----------------------------------------------------
	AMP = sqrt( aindex^2. + qindex^2. )
	PHS = atan2(qindex,aindex)
	;-----------------------------------------------------
	; Load RMM data
	;-----------------------------------------------------
	if isvar("infile") then delete(infile) end if
	ifile  = "~/Research/OBS/RMM/RMM.2000-2012.no_leap.nc" 
	infile = addfile(ifile,"r")
	;date = infile->yr*1e4+infile->mn*1e2+infile->dy
	yr = infile->yr
	RMMAMP = infile->AMP( ind((yr.ge.yr1).and.(yr.le.yr2)) )
	RMMPHS = infile->PHS( ind((yr.ge.yr1).and.(yr.le.yr2)) )	
;====================================================================================================
; Create Plot
;====================================================================================================
	ny = (yr2-yr1+1)
	nm = 4
	tmvals = new((/ny*12/nm,6/),integer)
	do y = 0,ny-1 tmvals(y*12/nm:(y+1)*12/nm-1,0) = yr1+y end do
	month = ispan(0,ny*12-1,1)%12+1
	tmvals(:,1) = month(::nm)
	tmvals(:,2) = 1
	tmvals(:,3) = 0
	tmvals(:,4) = 0
	tmvals(:,5) = 0

	
		cdtime = cd_calendar(time,2)
	  	tmres 					= True
	  	tmres@ttmFormat 		= "%D %c %Y"
	  	tmres@ttmAxis 			= "XB"
	  	tmres@ttmMajorStride 	= 30
	  	;tmres@ttmvalues 		= tmvals
	  	time_axis_labels(time,res,tmres)

		tres = res
		tres@xyLineThicknessF	= 3.
		tres@xyDashPatterns		= (/0,0,0,0/)
		tres@xyLineColors		= (/"red","blue"/)
		tres@xyLineColor		= "black"
		

		
		tres@gsnLeftString 		= "Standardized TPW Plateau Area (red) and Intensity (blue) Indices"
	plot(0) = gsn_csm_xy(wks,time,(/aindex,qindex/),tres)
		
		tres@xyMonoLineColor	= True
		tres@gsnLeftString 		= "TPW Plateau Index Amplitude"
		tres@gsnRightString		= "correlation: "+escorc(AMP,RMMAMP)
	plot(1) = gsn_csm_xy(wks,time,AMP,tres)
	
		tres@gsnLeftString 		= "TPW Plateau Index Phase"
		tres@gsnRightString		= ""
	plot(2) = gsn_csm_xy(wks,time,PHS,tres)
		
		delete(tres)
		tres = res
		tres@xyLineThicknessF	= 2.
		tres@xyLineColor		= "blue"
		tres@xyDashPattern		= 0
	overlay(plot(1) , gsn_csm_xy(wks,time,RMMAMP,tres))

	xx = (/-1e8,1e8/)
	do p = 0,dimsizes(plot)-1
	if .not.ismissing(plot(p)) then
		overlay(plot(p),gsn_csm_xy(wks,xx,xx*0.,lres))
	end if
	end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnPanelBottom                     = 0.1 
		pres@amJust                             = "TopLeft"
		pres@gsnPanelFigureStringsFontHeightF   = 0.015
		;pres@gsnPanelFigureStrings              = (/"a","b","c","d"/) 
		;pres@gsnPanelYWhiteSpacePercent         = 5

	gsn_panel(wks,plot,(/num_p,1/),pres)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
