; Plot a composite map of the equatorial area for each index phase
; to see how well index can capture the MJO structure
; v1 -> plots TPW and wind vectors, index calculated explicitly
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    fig_type = "png"
    fig_file = "~/Research/OBS/ERAi/figs_TPW/index.composite.v1"
    
    verbose = False
    debug   = True

    lat1 =  -15.
    lat2 =   15.
    lon1 =   40.
    lon2 =  180.

    plat1 = -30.
    plat2 =  30.

    threshold = 45.     ; Threshold TPW value for plataeu 
    nsmooth   = 2       ; amount to smooth the final indices
    num_p     = 6       ; number of phases

    addwind = False
    windlev = 850.
;====================================================================================================
; Load data
;====================================================================================================
    idir   = "~/Data/OBS/ERAi/data"
    ifile  = systemfunc("ls -1 "+idir+"/*.1.0-deg."+"CWV"+"*no_leap.nc")
    infile = addfiles(ifile,"r")

    time   = tofloat( infile[:]->time )
    time := time(::4)
    time := time
    time = (/ time-time(0) /)
    ;time@units = "hours since 2014-11-01 00:00:00"
    if debug then time := time(:365*2-1) end if

    lat = infile[0]->lat({plat1:plat2})
    lon = infile[0]->lon;({lon1:lon2})

    lat@units = "degrees north"
    lon@units = "degrees east"

    ntime = dimsizes(time)
    nlat  = dimsizes(lat)
    nlon  = dimsizes(lon)

    TPW  = block_avg( infile[:]->CWV(:,{plat1:plat2},:) ,4)
    TPW := TPW(:ntime-1,:,:)
    ;-----------------------------------------------------
    ; Load wind data
    ;-----------------------------------------------------
    if addwind then
        Ufile = addfiles(systemfunc("ls -1 "+idir+"/*.1.0-deg."+"U"+"*no_leap.nc"),"r")
        Vfile = addfiles(systemfunc("ls -1 "+idir+"/*.1.0-deg."+"V"+"*no_leap.nc"),"r")
        U = block_avg( Ufile[:]->U(:,{windlev},{plat1:plat2},:) ,4)
        V = block_avg( Vfile[:]->V(:,{windlev},{plat1:plat2},:) ,4)
        U := U(:ntime-1,:,:)
        V := V(:ntime-1,:,:)
    end if
    ;-----------------------------------------------------
    ; Remove seasonal cycle
    ;-----------------------------------------------------
    ; calculate average seasonal cycle
    sTPW  = new((/365,nlat,nlon/),float)
    do d = 0,364 sTPW(d,:,:) = dim_avg_n(TPW(d::365,:,:),0) end do
    ; Calculate harmonics of seasonal cycle
    num_h = 3
    H = new((/365,nlat,nlon/),float)
    H = 0.
    do h = 0,num_h-1 H = (/ H + calcHarmonic(sTPW,h) /) end do
    hvals = ispan(0,ntime-1,1)%365
    ; remove first 3 harmonics opf seasonal cycle
    TPW = (/ TPW - H(hvals,:,:) /)
    ; print TPW stats for sanity check
    printline()
    printMAMS(TPW)
    printline()
    ;-----------------------------------------------------
    ; Select season
    ;-----------------------------------------------------
    doy    = conform(TPW,ispan(0,ntime-1,1)%365,0)
    winter = new(dimsizes(TPW),logical)
    summer = new(dimsizes(TPW),logical)
    winter = False
    summer = False
    ; winter ~ NDJFM , summer ~ MJJAS
    ;winter = where( doy.lt.(31+28+31),True,winter)
    ;winter = where( doy.ge.(31+28+31+30+31+30+31+31+30+31),True,winter)
    ;summer = where( doy.ge.(31+28+31+30),True,winter)
    ;summer = where( doy.lt.(31+28+31+30+31+30+31+31+30),True,winter)
    ; winter ~ ONDJF , summer ~ AMJJA
    winter = where( doy.lt.(31+28),True,winter)
    winter = where( doy.ge.(31+28+31+30+31+30+31+31+30),True,winter)
    summer = where( doy.ge.(31+28+31),True,winter)
    summer = where( doy.lt.(31+28+31+30+31+30+31+31),True,winter)

    TPW = where(winter,TPW,TPW@_FillValue)
    ;-----------------------------------------------------
    ; Initialize variables for index
    ;-----------------------------------------------------
    iTPW = TPW(:,{lat1:lat2},{lon1:lon2})
    aindex = new(ntime,float)
    qindex = new(ntime,float)
    num_pts = new(ntime,float)
    num_pts = dim_num_n(.not.ismissing(iTPW),(/1,2/))
    num_pts = where(num_pts.ne.0,num_pts,num_pts@_FillValue)
    ;-----------------------------------------------------
    ; Create area index
    ;-----------------------------------------------------
    aindex = tofloat( dim_num_n(iTPW.gt.threshold,(/1,2/)) )
    aindex = aindex / tofloat( num_pts )
    aindex = aindex - avg(aindex)
    aindex = aindex/stddev(aindex)  
    aindex!0    = "time"
    aindex&time = iTPW&time
    ;-----------------------------------------------------
    ; Create plateau "intensity" or "value" index
    ;-----------------------------------------------------
    qindex = tofloat( dim_avg_n(where(iTPW.gt.threshold,iTPW,iTPW@_FillValue),(/1,2/)) )
    qindex = qindex - avg(qindex)
    qindex = qindex/stddev(qindex)  
    qindex!0    = "time"
    qindex&time = iTPW&time
    ;-----------------------------------------------------
    ; Screen for times when a lot of data is missing
    ;-----------------------------------------------------
    num_pts_iTPW = product(dimsizes(iTPW(0,:,:)))
    aindex = where(num_pts.gt.num_pts_iTPW*0.5,aindex,aindex@_FillValue)
    qindex = where(num_pts.gt.num_pts_iTPW*0.5,qindex,qindex@_FillValue)

    do n = 0,nsmooth-1
        aindex = smooth(aindex)
        qindex = smooth(qindex)
    end do
    ;-----------------------------------------------------
    ; Calculate Amplitude and phase of new index
    ;-----------------------------------------------------
    AMP = sqrt( aindex^2. + qindex^2. )
    PHS = atan2( qindex , aindex )
;====================================================================================================
; create composites
;====================================================================================================
    dp = 2.*pi / num_p
    rphs = fspan(-pi,pi-dp,num_p)
    dphs = rphs*180./pi
    ;-----------------------------------------------------
    ; initialize composite variable
    ;-----------------------------------------------------
    comp = new((/num_p,nlat,nlon/),float)
    comp!0 = "phase"
    comp!1 = "lat"
    comp!2 = "lon"
    comp&lat = lat
    comp&lon = lon
    ;-----------------------------------------------------
    ; Remove TPW mean (better for plotting here)
    ;-----------------------------------------------------
    TPW = dim_rmvmean_n_Wrap(TPW,0)
    ;-----------------------------------------------------
    ; build composite
    ;-----------------------------------------------------
    printline()
    do p = 0,num_p-1
        condition = (PHS.ge.(rphs(p)-dp/2.)).and.(PHS.lt.(rphs(p)+dp/2.)).and.(AMP.gt.1.0)
        print(p+"  "+sprintf("%6.3f",rphs(p))+"  # = "+num(condition))
        tmp = TPW
        comp(p,:,:) = dim_avg_n( where(conform(tmp,condition,0),tmp,tmp@_FillValue) ,0)
        delete([/condition,tmp/])
    end do
    printline()
;====================================================================================================
;====================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(num_p,graphic)
        res = setres_contour()
        res@tmXBOn          = False
        res@tmYLOn          = False
        res@mpLimitMode     = "LatLon"
        res@mpMinLatF       = -30
        res@mpMaxLatF       =  30
        res@mpCenterLonF    = 180.
        res@cnLineLabelsOn  = False
        res@cnInfoLabelOn   = False
        res@gsnLeftStringFontHeightF  = 0.015
        res@gsnRightStringFontHeightF = 0.015
        res@cnLevelSelectionMode = "ExplicitLevels"
        
        dc = 4
        res@cnLevels        = fspan(-dc,dc,dc*2+1)
        ;res@cnLevels        = ispan(20,60,4)
        ;res@cnLevels        = fspan(threshold-8,threshold+8,11)
    ;-----------------------------------------------------
    ; Plot each composite
    ;-----------------------------------------------------
    do p = 0,num_p-1
            res@gsnLeftString   = "Phase "+p+"  ("+dphs(p)+" deg)"
        plot(p) = gsn_csm_contour_map(wks,comp(p,:,:),res)
    end do
    ;-----------------------------------------------------
    ; add horzontal lines
    ;-----------------------------------------------------
    lres = setres_default()
    lres@xyLineThicknessF = 4.
    do p = 0,dimsizes(plot)-1
        xx = (/-1e8,1e8/)
        yy = (/1.,1./)
        if .not.ismissing(plot(p)) then
            overlay(plot(p),gsn_csm_xy(wks,xx,yy*0,lres))
        end if
    end do
    ;-----------------------------------------------------
    ; add box around index area
    ;-----------------------------------------------------
    gres = True
    gres@gsLineColor        = "black"
    gres@gsLineThicknessF   = 4.
    gx = (/lon1,lon1,lon2,lon2,lon1/)
    gy = (/lat1,lat2,lat2,lat1,lat1/)
    gdum = new(num_p,graphic)
    do p = 0,dimsizes(plot)-1
        gdum(p) = gsn_add_polyline(wks,plot(p),gx,gy,gres)
    end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
    pres = setres_panel_labeled() 

    gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

    trimPNG(fig_file)
  
;====================================================================================================
;====================================================================================================
end