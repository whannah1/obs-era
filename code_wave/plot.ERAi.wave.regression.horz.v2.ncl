load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/diagnostics_cam.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/kf_filter.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin 
	
	waveName = "kelvin" 
	
	fig_type = "png"
	fig_file = "~/Research/OBS/ERAi/figs_wave/ERAi.wave.regression.v2."+waveName
	
	recalc = True
	
	tfile = "~/Data/OBS/ERAi/tmp.ERAi.wave.regression.v2."+waveName+".nc"
	
;=========================================================================================================
;=========================================================================================================	
	
	lat1 = -30.
	lat2 =  30.
	
	;lon1 =  50.
	;lon2 = 200.
	
	clon = 180.
	
	dlon = 50.
	lon1 = clon - dlon
	lon2 = clon + dlon
	
	windlev = 850
	zlev    = 850
	
	idir = "~/Data/OBS/ERAi/data"
    fstr = "2*"
    
    obsPerDay = 1
;=========================================================================================================
; Define parameters for wave types
;=========================================================================================================
      if waveName.eq."kelvin" then
        tMin = 2.5
        tMax = 20
        kMin = 1
        kMax = 14
        hMin = 8
        hMax = 200
      end if
      if waveName.eq."ER" then
        tMin = 2.5
        tMax = 40
        kMin = -1
        kMax = -10
        hMin = 4
        hMax = 90
      end if
      if waveName.eq."MJO_WEST" then
        tMin = 20
        tMax = 90
        kMin = -1
        kMax = -8
        hMin = 8
        hMax = 90
      end if
      if waveName.eq."MJO_EAST" then
        tMin = 20
        tMax = 90
        kMin = 1
        kMax = 8
        hMin = 8
        hMax = 90
      end if
;=========================================================================================================
; Load OLR data
;=========================================================================================================
    ;infile = addfile("~/Data/OBS/OLR/olr.daily.1990-2012.nc","r")
    infile = addfile("~/Data/OBS/OLR/olr.daily.2000-2012.nc","r")
    OLR = infile->OLR(:,{lat1:lat2},:)
    OLRlat = infile->lat({lat1:lat2})
    OLRlon = infile->lon({lon1:lon2})
    
    flat1 = -15.
    flat2 =  15.
    
    fOLR = kf_filter(dim_avg_n_Wrap(OLR(:,{flat1:flat2},:),1),obsPerDay,tMin,tMax,kMin,kMax,hMin,hMax,waveName)
    fOLR = (/fOLR*-1./)
    fOLR@long_name = "filtered OLR"
    
    OLR  := OLR(:,:,{lon1:lon2})
    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
    printline()
    printVarSummary(OLR)
    printline()
    ;----------------------------------------------------------------------
    ; Calculate anomalies
    ;----------------------------------------------------------------------
    OLR = dim_rmvmean_n_Wrap(OLR,0)
    ;----------------------------------------------------------------------
    ; Create composite fields
    ;----------------------------------------------------------------------
    index = fOLR(:,{clon})
    index = index / stddev(index)
    cOLR = regCoef( index, OLR(lat|:,lon|:,time|:))
    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
    tcrit = 1.96
    dims = dimsizes(cOLR)
    cOLR = where( abs(reshape(cOLR@tval,dims)).gt.tcrit,cOLR,cOLR@_FillValue )
    ;----------------------------------------------------------------------
    ;----------------------------------------------------------------------
    copy_VarCoords(OLR(0,:,:),cOLR)
    cOLR!0 = "OLRlat"
    cOLR!1 = "OLRlon"
;=========================================================================================================
; Create Plot
;=========================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    gsn_define_colormap(wks,"WhBlGrYeRe")
    gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
      	res = setres_contour()
      	res@cnInfoLabelOn 	= False
      	res@cnLineLabelsOn 	= False
      	;res@gsnLeftStringFontHeightF  	= 0.02
      	
      	tres = res
      	tres@gsnAddCyclic      = False
      	tres@cnFillPalette = "BlueWhiteOrangeRed"
      	buffer = 10.
      	tres@mpCenterLonF   = clon
	    tres@mpMinLatF 		= lat1 - buffer
	    tres@mpMaxLatF 		= lat2 + buffer
	    tres@mpMinLonF      = lon1 - buffer
	    tres@mpMaxLonF      = lon2 + buffer
	    
	    symMinMaxPlt(cOLR,15,False,tres)

    plot = gsn_csm_contour_map(wks,cOLR,tres)
    
    ;tval = reshape(cOLR@tval,dims)
    ;copy_VarCoords(cOLR,tval)
    ;plot = gsn_csm_contour_map(wks,tval,tres)
    
;====================================================================================================
;====================================================================================================
	    pres = True	
	
    gsn_panel(wks,plot,(/1,1/),pres)
 
    trimPNG(fig_file)
  
;====================================================================================================
;====================================================================================================
end


