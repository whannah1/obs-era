load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/diagnostics_cam.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/kf_filter.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin 
	
	waveName = "MJO_EAST" 
	
	fig_type = "png"
	fig_file = "~/Research/OBS/ERAi/figs_wave/ERAi.wave.regression.v1."+waveName
	
	recalc  = True
	verbose = True
	
	tfile = "~/Data/OBS/ERAi/tmp.ERAi.wave.regression.v1."+waveName+".nc"
	
;=========================================================================================================
;=========================================================================================================	
	
	lat1 = -30.
	lat2 =  30.
	
	;lon1 =  50.
	;lon2 = 200.
	
	clon = 80.
	
	dlon = 80.
	lon1 = clon - dlon
	lon2 = clon + dlon
	
	windlev = 850
	zlev    = 950
	
	idir = "~/Data/OBS/ERAi/data"
    fstr = "2*"
    
    obsPerDay = 1
;=========================================================================================================
; Define parameters for wave types
;=========================================================================================================
      if waveName.eq."kelvin" then
        tMin = 2.5
        tMax = 20
        kMin = 1
        kMax = 14
        hMin = 8
        hMax = 200
      end if
      if waveName.eq."ER" then
        tMin = 2.5
        tMax = 40
        kMin = -1
        kMax = -10
        hMin = 4
        hMax = 90
      end if
      if waveName.eq."MJO_WEST" then
        tMin = 20
        tMax = 90
        kMin = -1
        kMax = -8
        hMin = 8
        hMax = 90
      end if
      if waveName.eq."MJO_EAST" then
        tMin = 20
        tMax = 90
        kMin = 1
        kMax = 8
        hMin = 8
        hMax = 90
      end if
    if verbose then print("") end if
;=========================================================================================================
; Load OLR data
;=========================================================================================================
    if recalc then
        if verbose then print("loading OLR...") end if
        ;infile = addfile("~/Data/OBS/OLR/olr.daily.1990-2012.nc","r")
        infile = addfile("~/Data/OBS/OLR/olr.daily.2000-2012.nc","r")
        OLR = infile->OLR(:,{lat1:lat2},:)
        OLRlat = infile->lat({lat1:lat2})
        OLRlon = infile->lon({lon1:lon2})
        
        flat1 = -15.
        flat2 =  15.
        
        fOLR = kf_filter(dim_avg_n_Wrap(OLR(:,{flat1:flat2},:),1),obsPerDay,tMin,tMax,kMin,kMax,hMin,hMax,waveName)
        fOLR = (/fOLR*-1./)
        fOLR@long_name = "filtered OLR"
        
        OLR  := OLR(:,:,{lon1:lon2})
        
        if False then
            ;files  := systemfunc("ls "+idir+"/ERAi.6hour.1.0-deg.U."+fstr+".no_leap.nc")
            ;infile := addfiles(files,"r")
            ;tU = block_avg( infile[:]->U(:,{windlev},{lat1:lat2},:) ,4)
            ;fU = kf_filter(dim_avg_n_Wrap(tU(:,{flat1:flat2},:),1),obsPerDay,tMin,tMax,kMin,kMax,hMin,hMax,waveName)
            ;fU = (/fU*-1./)
            ;delete(tU)
        end if
        if False then         
            files  := systemfunc("ls "+idir+"/ERAi.6hour.1.0-deg.GEO."+fstr+".no_leap.nc")
            infile := addfiles(files,"r")
            tZ = block_avg( infile[:]->GEO(:,{zlev},{lat1:lat2},:) ,4)
            fZ = kf_filter(dim_avg_n_Wrap(tZ(:,{flat1:flat2},:),1),obsPerDay,tMin,tMax,kMin,kMax,hMin,hMax,waveName)
            fZ = (/fZ*-1./)
            delete(tZ)
        end if
    end if
;=========================================================================================================
; Load ERAi data
;=========================================================================================================
    if recalc then
        ;----------------------------------------------------------------------
        ;----------------------------------------------------------------------
        if verbose then print("loading U...") end if
        files  := systemfunc("ls "+idir+"/ERAi.6hour.1.0-deg.U."+fstr+".no_leap.nc")
        infile := addfiles(files,"r")
        U = block_avg( infile[:]->U(:,{windlev},{lat1:lat2},{lon1:lon2}) ,4)
        
        if verbose then print("loading V...") end if
        files  := systemfunc("ls "+idir+"/ERAi.6hour.1.0-deg.V."+fstr+".no_leap.nc")
        infile := addfiles(files,"r")
        V = block_avg( infile[:]->V(:,{windlev},{lat1:lat2},{lon1:lon2}) ,4)
        
        if verbose then print("loading Z...") end if
        files  := systemfunc("ls "+idir+"/ERAi.6hour.1.0-deg.GEO."+fstr+".no_leap.nc")
        infile := addfiles(files,"r")
        Z = block_avg( infile[:]->GEO(:,{zlev},{lat1:lat2},{lon1:lon2}) ,4)
        
        if verbose then print("loading Psfc...") end if
        files  := systemfunc("ls "+idir+"/ERAi.6hour.1.0-deg.Ps."+fstr+".no_leap.nc")
        infile := addfiles(files,"r")
        Ps = block_avg( infile[:]->Ps(:,{lat1:lat2},{lon1:lon2}) ,4)
        
        lat = infile[0]->lat({lat1:lat2})
        lon = infile[0]->lon({lon1:lon2})
        ;----------------------------------------------------------------------
        ;----------------------------------------------------------------------
        ;printVarSummary(OLR)
        ;printline()
        ;printVarSummary(U)
        ;printline()
        ;----------------------------------------------------------------------
        ; Calculate anomalies
        ;----------------------------------------------------------------------
        if verbose then print("Calculating anomalies...") end if
        OLR = dim_rmvmean_n_Wrap(OLR,0)
        U   = dim_rmvmean_n_Wrap(U  ,0)
        V   = dim_rmvmean_n_Wrap(V  ,0)
        Z   = dim_rmvmean_n_Wrap(Z  ,0)
        Ps  = dim_rmvmean_n_Wrap(Ps ,0)
        ;----------------------------------------------------------------------
        ; Create composite fields
        ;----------------------------------------------------------------------
        if verbose then print("Calculating regression coefficients...") end if
        index = fOLR(:,{clon})
        ;index = fZ  (:,{clon})
        index = index / stddev(index)
        cOLR = regCoef( index, OLR(lat|:,lon|:,time|:))
        cU   = regCoef( index, U  (lat|:,lon|:,time|:))
        cV   = regCoef( index, V  (lat|:,lon|:,time|:))
        cZ   = regCoef( index, Z  (lat|:,lon|:,time|:))
        cPs  = regCoef( index, Ps (lat|:,lon|:,time|:))
        ;----------------------------------------------------------------------
        ;----------------------------------------------------------------------
        ;cOLR!0 = "lat"
        ;cOLR!1 = "lon"
        ;cOLR&lat = lat
        ;cOLR&lon = lon
        copy_VarCoords(OLR(0,:,:),cOLR)
        copy_VarCoords(U(0,:,:),cU)
        copy_VarCoords(U(0,:,:),cV)
        copy_VarCoords(U(0,:,:),cZ)
        copy_VarCoords(U(0,:,:),cPs)
        cOLR!0 = "OLRlat"
        cOLR!1 = "OLRlon"
        ;----------------------------------------------------------------------
        ;----------------------------------------------------------------------
        if verbose then print("Writing temporary file...") end if
        if isfilepresent(tfile) then system("rm "+tfile) end if
        outfile = addfile(tfile,"c")
        outfile->cU   = cU
        outfile->cV   = cV
        outfile->cZ   = cZ
        outfile->cPs  = cPs
        outfile->cOLR = cOLR
        outfile->windlev = windlev
        outfile->clon    = clon
        ;----------------------------------------------------------------------
        ;----------------------------------------------------------------------
    else
        infile := addfile(tfile,"r")
        cOLR = infile->cOLR
        cU   = infile->cU
        cV   = infile->cV
        cZ   = infile->cZ
        cPs  = infile->cPs
    end if
    printline()
;=========================================================================================================
;=========================================================================================================
    tcrit = 1.96
    if True then
        if verbose then print("Screening points for statistical significance...") end if
        
        dims = dimsizes(cOLR)
        cOLR = where( abs(reshape(cOLR@tval,dims)).gt.tcrit,cOLR,cOLR@_FillValue )
        
        dims = dimsizes(cU)
        cU   = where( abs(reshape( cU@tval,dims)).gt.tcrit, cU, cU@_FillValue )
        cV   = where( abs(reshape( cV@tval,dims)).gt.tcrit, cV, cV@_FillValue )
        cZ   = where( abs(reshape( cZ@tval,dims)).gt.tcrit, cZ, cZ@_FillValue )
        cPs  = where( abs(reshape(cPs@tval,dims)).gt.tcrit,cPs,cPs@_FillValue )
        
        printline()
    end if
;=========================================================================================================
; Create Plot
;=========================================================================================================
    if verbose then print("Creating plot...") end if
    wks = gsn_open_wks(fig_type,fig_file)
    gsn_define_colormap(wks,"WhBlGrYeRe")
    gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
      	res = setres_contour()
      	res@cnInfoLabelOn 	= False
      	res@cnLineLabelsOn 	= False
      	;res@gsnLeftStringFontHeightF  	= 0.02
      	
      	tres = res
      	tres@gsnAddCyclic      = False
      	tres@cnFillPalette = "BlueWhiteOrangeRed"
      	buffer = 10.
      	tres@mpCenterLonF   = clon
	    tres@mpMinLatF 		= lat1 - buffer
	    tres@mpMaxLatF 		= lat2 + buffer
	    tres@mpMinLonF      = lon1 - buffer
	    tres@mpMaxLonF      = lon2 + buffer
	    
	    symMinMaxPlt(cOLR,15,False,tres)

    plot = gsn_csm_contour_map(wks,cOLR,tres)
    
        res@cnFillOn    = False
        res@cnLinesOn   = True
        res@cnLineThicknessF             = 3.
        res@gsnContourZeroLineThicknessF = 6.
        res@gsnContourNegLineDashPattern = 2
        res@gsnLeftString = ""
    overlay(plot , gsn_csm_contour(wks,cZ,res))
    
        res := setres_vector()
        res@vcMinDistanceF   = 0.01
        ;res@vcRefMagnitudeF  = 0.5
        res@vcRefLengthF     = 0.05
    overlay(plot , gsn_csm_vector(wks,cU,cV,res))
;====================================================================================================
;====================================================================================================
	    pres = True	
	
    gsn_panel(wks,plot,(/1,1/),pres)
 
    trimPNG(fig_file)
  
;====================================================================================================
;====================================================================================================
end


