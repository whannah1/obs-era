#!/usr/bin/env python3
# Script for downloading ERA5 pressure level and surface data
# links to CDS web interface:
#   https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels
#   https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels
# A list of available variables can also be found in the ERA5 documentation:
#   https://confluence.ecmwf.int/display/CKB/ERA5%3A+data+documentation
import os, cdsapi, datetime
server = cdsapi.Client()
#-------------------------------------------------------------------------------
class tcolor:
  ENDC = '\033[0m';  BLACK     = '\033[30m'; RED  = '\033[31m'; GREEN = '\033[32m'
  BLUE = '\033[34m'; MAGENTA   = '\033[35m'; CYAN = '\033[36m'; WHITE = '\033[37m'
  BOLD = '\033[1m';  UNDERLINE = '\033[4m'
#-------------------------------------------------------------------------------
get_atm, get_sfc, get_lnd = False, False, False

get_atm = True
# get_sfc = True
# get_lnd = True

monthly = False



# yr_list   = [f'{i:04d}' for i in range(1950,1959+1)]
# yr_list   = [f'{i:04d}' for i in range(1960,1969+1)]
# yr_list   = [f'{i:04d}' for i in range(1970,2014+1)]
# yr_list   = [f'{i:04d}' for i in range(2000,2019+1)]

# All months, days, and times
# mn_list   = [f'{i:02d}' for i in range(1,12+1)]
# dy_list   = [f'{i:02d}' for i in range(1,31+1)]
# time_list = [f'{i:02d}:00' for i in range(0,23+1)]

### 5-day dataset for KPP verification
# yr_list,mn_list,dy_list = [1950],[1],[f'{i:02d}' for i in range(1,5+1)]
# yr_list,mn_list,dy_list = [1960],[1],[f'{i:02d}' for i in range(1,5+1)]
# yr_list,mn_list,dy_list = [1990],[1],[f'{i:02d}' for i in range(1,5+1)]

#-------------------------------------------------------------------------------
def add_yrmndy(yrmndy_list,yr1,mn1,dy1,yr2,mn2,dy2):
  done = False
  cnt = 0
  # yrmndy_beg = int(yr1*1e4+mn1*1e2+dy1)
  # yrmndy_end = int(yr2*1e4+mn2*1e2+dy2)
  beg_date = datetime.date(yr1, mn1, dy1)
  end_date = datetime.date(yr2, mn2, dy2)
  while not done:
    cur_date = beg_date + datetime.timedelta(days=cnt)
    # day_of_year = cur_date.timetuple().tm_yday
    yrmndy = int( cur_date.year*1e4 + cur_date.month*1e2 + cur_date.day )
    yrmndy_list.append(yrmndy)
    if cur_date==end_date: done = True
    cnt+=1
#-------------------------------------------------------------------------------
yrmndy_list = []

# SCREAMv1 paper
# yr1,mn1,dy1=2016, 8, 1; yr2,mn2,dy2=2016, 9, 9 ; add_yrmndy(yrmndy_list,yr1,mn1,dy1,yr2,mn2,dy2)
yr1,mn1,dy1=2013, 4, 1; yr2,mn2,dy2=2013, 5,10 ; add_yrmndy(yrmndy_list,yr1,mn1,dy1,yr2,mn2,dy2)
# yr1,mn1,dy1=2013,10, 1; yr2,mn2,dy2=2013,11, 9 ; add_yrmndy(yrmndy_list,yr1,mn1,dy1,yr2,mn2,dy2)
# yr1,mn1,dy1=2020, 1,20; yr2,mn2,dy2=2020, 2,28 ; add_yrmndy(yrmndy_list,yr1,mn1,dy1,yr2,mn2,dy2)

# print(yrmndy_list)
# exit()

# time_list = [f'{n:02d}:00' for n in range(0,24,1)] # 1-hourly
time_list = [f'{n:02d}:00' for n in range(0,24,3)] # 3-hourly

# lev = [ '50', '70','100','125','150','175','200','225','250','300','350'
#       ,'400','450','500','550','600','650','700','750','775','800','825'
#       ,'850','875','900','925','950','975','1000']

### Small level set for testing
# lev = [ '50','100','150','200','300','400','500','600'
#       ,'700','750','800','850','900','950','1000']


### levels for SCREAM wind analysis
# lev = ['1','5','10','20','30','50','70','100','150','200','250','300','400','500','600','700','850','925','1000']
lev = ['200','500','850']


### all levels
# lev=['1','2','3','5','7','10','20','30','50','70','100',
#      '125','150','175','200','225','250','300','350','400',
#      '450','500','550','600','650','700','750','775','800',
#      '825','850','875','900','925','950','975','1000']

# lev = ['850']

# output_path = os.getenv('PWD')+'/data/'
# output_path = os.getenv('HOME')+"/Data/Obs/ERA5/"
output_path = '/pscratch/sd/w/whannah/Obs/ERA5'         # NERSC
# output_path = '/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5'  # OLCF

if monthly: time_list = ['00:00']

#-------------------------------------------------------------------------------
# for yr in yr_list:
#   for mn in mn_list:
for yrmndy in yrmndy_list:
    yr = int(yrmndy/1e4)
    mn = int((yrmndy-yr*1e4)/1e2)
    dy = int(yrmndy-yr*1e4-mn*1e2)
    # cur_date = datetime.date(yr, mn, dy)
    # day_of_year = cur_date.timetuple().tm_yday

    # Specify base file name
    if monthly:
        output_file_base = f'{output_path}/monthly/ERA5.monthly'
    else:
        # output_file_base = f'{output_path}/daily/ERA5.daily'
        # output_file_base = f'{output_path}/daily/ERA5.daily.U'
        output_file_base = f'{output_path}/daily/ERA5.daily.Z'
    # specify specifi file names
    # output_file_plv = output_file_base+f'.atm.{yr}.nc'
    # output_file_sfc = output_file_base+f'.sfc.{yr}.nc'
    # output_file_lnd = output_file_base+f'.lnd.{yr}.nc'
    output_file_plv = output_file_base+f'.atm.{yr}-{mn:02d}-{dy:02d}.nc'
    output_file_sfc = output_file_base+f'.sfc.{yr}-{mn:02d}-{dy:02d}.nc'
    output_file_lnd = output_file_base+f'.lnd.{yr}-{mn:02d}-{dy:02d}.nc'
    #-------------------------------------------------------------------------------
    # atmosphere pressure level data
    if get_atm:
        if monthly:
            dataset = 'reanalysis-era5-pressure-levels-monthly-means'
            product = 'monthly_averaged_reanalysis'
        else:
            dataset = 'reanalysis-era5-pressure-levels'
            product = 'reanalysis'
        server.retrieve(dataset,{
            'product_type'  : product,
            'pressure_level': lev,
            'time'          : time_list,
            # 'day'           : dy_list,
            # 'month'         : mn_list,
            'day'           : dy,
            'month'         : mn,
            'year'          : yr,
            'format'        : 'netcdf',
            'variable'      : [
                              # 'temperature',
                              # 'specific_humidity',
                              'geopotential',
                              # 'u_component_of_wind',
                              # 'v_component_of_wind',
                              # 'vertical_velocity',
                              # ,'ozone_mass_mixing_ratio'
                              # ,'specific_cloud_ice_water_content'
                              # ,'specific_cloud_liquid_water_content'
                              ],
        }, output_file_plv)
    #-------------------------------------------------------------------------------
    # "single level" / surface data
    if get_sfc:

        # output_file_sfc = f'{output_path}/monthly/ERA5.monthly.land-sea-mask.{yr}-{mn}.nc'
        
        if monthly:
            dataset = 'reanalysis-era5-single-levels-monthly-means'
            product = 'monthly_averaged_reanalysis'
        else:
            dataset = 'reanalysis-era5-single-levels'
            product = 'reanalysis'
        server.retrieve(dataset,{
            'product_type'  : product,
            'time'          : time_list,
            'day'           : dy_list,
            # 'month'         : mn_list,
            'month'         : mn,
            'year'          : yr,
            'format'        : 'netcdf',
            'variable'      : [
                              'surface_pressure',
                              'skin_temperature',
                              # 'total_column_cloud_liquid_water',
                              # 'total_column_cloud_ice_water',
                              # 'total_column_water',
                              # 'top_net_solar_radiation',
                              # 'top_net_thermal_radiation',
                              # 'mean_surface_sensible_heat_flux',
                              # 'mean_surface_latent_heat_flux',
                              # 'total_precipitation',
                              # '',
                              # '',
                              # '',
                              # '',
                              # 'sea_surface_temperature',
                              # 'soil_temperature_level_1',
                              # 'soil_temperature_level_2',
                              # 'soil_temperature_level_3',
                              # 'soil_temperature_level_4',
                              # 'leaf_area_index_high_vegetation',
                              # 'leaf_area_index_low_vegetation',
                              # 'skin_reservoir_content',
                              # 'snow_albedo',
                              # 'snow_density',
                              # 'snow_depth',
                              # 'temperature_of_snow_layer',
                              # 'volumetric_soil_water_layer_1',
                              # 'volumetric_soil_water_layer_2',
                              # 'volumetric_soil_water_layer_3',
                              # 'volumetric_soil_water_layer_4',
                              # 'orography',
                              # 'sea_ice_cover',
                              ],
        }, output_file_sfc)
#-------------------------------------------------------------------------------
