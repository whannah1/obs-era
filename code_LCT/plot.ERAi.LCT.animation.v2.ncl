; Plot animation frames of the time mean Lagrangian CWV tendency 
; v1 - basic version - plan view map
; v2 - height vs. latitude view
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/diagnostics_cam.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/kf_filter.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
load "$NCARG_ROOT/custom_functions_ERAi.ncl"
begin 
    odir = "~/Research/Obs/ERAi/"

    fig_type = "png"
    fig_stub = "ERAi.LCT.animation.v2"

    yr = 2015
    mn = 5
    
    debug   = False      ; debug mode
    add_vec = False     ; add wind vectors

;=========================================================================================================
;=========================================================================================================  
    if debug then fig_type = "x11" end if

    windlev = 850   ; level for vector overlay
    
    lat1 = -20
    lat2 =  20
    lon1 = 60
    lon2 = 70

    ;ERAi_resolution = "0.25-deg"

;=========================================================================================================
;=========================================================================================================
    idir = "~/Data/Obs/ERAi/data/"

    leap_flag = ""
    ;leap_flag = ".noleap"

    infile = addfile(idir+"ERAi.6hour.0.25-deg.Q."+yr+leap_flag+".nc","r")
    nt = dimsizes(infile->time)

    if debug then nt = 4 * 3 end if     ; 4 x (# days)

    time = infile->time(:nt-1)
    lat = infile->lat
    ;--------------------------------------
    ; Load Q,U,V
    ;--------------------------------------
    infile = addfile(idir+"ERAi.6hour.0.25-deg.U."+yr+leap_flag+".nc","r")
    U = infile->U(:nt-1,:,:,:)
    
    infile = addfile(idir+"ERAi.6hour.0.25-deg.V."+yr+leap_flag+".nc","r")
    V = infile->V(:nt-1,:,:,:)

    infile = addfile(idir+"ERAi.6hour.0.25-deg.Q."+yr+leap_flag+".nc","r")
    Q = infile->Q(:nt-1,:,:,:)

    infile = addfile(idir+"ERAi.6hour.0.25-deg.CWV."+yr+leap_flag+".nc","r")
    CWV = dim_avg_n_Wrap( infile->CWV(:nt-1,:,:) ,2)

    lev   = Q&lev
    time := Q&time
    ;--------------------------------------
    ; dP for vertical interpolation
    ;--------------------------------------
    ; infile = addfile(idir+"ERAi.6hour.0.25-deg.Ps."+yr+leap_flag+".nc","r")
    ; Ps = infile->Ps(:nt-1,:,:)
    ; P   = conform(Q,lev,1)*100.
    ; Po  = 100000.
    ; P!1 = "lev"
    ; P&lev = lev
    ; P@units = "Pa"
    ; dP = calc_dP(P,Ps)
    ; delete(Ps)
    ;--------------------------------------
    ;--------------------------------------
    ;CWV = dim_sum_n( Q*dP/g ,1)
    ;copy_VarCoords(Q(:,0,:,:),CWV)

    dt = 6.*3600.
    DDTQ = dim_avg_n( calc_ddt(Q,dt) ,3)
    UADV = dim_avg_n( U*calc_ddx(Q)  ,3)
    VADV = dim_avg_n( V*calc_ddy(Q)  ,3) 

    LCT = ( DDTQ + UADV + VADV ) * 86400. * 1e3 ;/ dim_sum_n( dP/g ,1)

    delete([/DDTQ,UADV,VADV/])
    delete([/U,V/])

    tQ = dim_avg_n_Wrap(Q,3)
    Q := tQ
    delete(tQ) 



    copy_VarCoords(Q,LCT)
    ;--------------------------------------
    ;--------------------------------------
    tQ   = block_avg(Q  ,4)
    tCWV = block_avg(CWV,4)
    tLCT = block_avg(LCT,4)
    Q   := tQ
    CWV := tCWV
    LCT := tLCT
    delete([/tQ,tCWV,tLCT/])

    time := time(::4)
    nt = dimsizes(time)

    ;fmt = "%Y-%N-%D %HUTC"
    fmt = "%Y-%N-%D"
    date = cd_string(time,fmt)
    ;--------------------------------------
    ;--------------------------------------

    ;CWV = (/ CWV - 50. /)
    Q = (/ Q*1e3 - 10. /)

;=========================================================================================================
; Create Plots and animate
;=========================================================================================================
nt = dimsizes(time)
do t = 1,nt-1
    st = sprinti("%3.3i",t)
    fig_file = odir+"animation_frames/"+fig_stub+"."+st
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(1,graphic)
        res = setres_contour_fill()
        res@tmXBLabelFontHeightF        = 0.01
        res@tmYLLabelFontHeightF        = 0.01
        res@gsnLeftStringFontHeightF    = 0.015
        res@gsnRightStringFontHeightF   = 0.015
        ;res@gsnCenterStringFontHeightF  = 0.01
        res@gsnYAxisIrregular2Linear    = True
        res@trYReverse                  = True
        res@cnLevelSelectionMode        = "ExplicitLevels"
        res@trYMinF = 400
        offset = 0.2
        res@lbBottomMarginF = 0.05 - offset
        res@lbTopMarginF    = 0.05 + offset
        
        tres := res
        
        tres@cnFillPalette      = "BlueWhiteOrangeRed"
        tres@gsnLeftString       = "LCT (shaded) / Q (contours)"
        tres@gsnRightString      = ""
        tres@cnLevels           := ispan(-24,24,2)/1e1
        ;symMinMaxPlt(LCT,21,False,tres)
    plot(0) = gsn_csm_contour(wks,LCT(t,:,:),tres)
    
    
        tres := res
        tres@cnFillOn       = False
        tres@cnLinesOn      = True
        tres@cnInfoLabelOn  = False
        ;tres@cnFillPalette      = "ncl_default"
        tres@gsnLeftString       = ""
        tres@gsnRightString      = ""
        tres@gsnContourNegLineDashPattern  = 1
        tres@gsnContourZeroLineThicknessF  = 6
        ;tres@cnLevels        = ispan(20,65,5)
        ;tres@cnLevels        = ispan(0,60,5)
    overlay(plot(0) , gsn_csm_contour(wks,Q(t,:,:),tres))


        pgres = True
        pgres@gsFillColor       = "black"
        pgres@gsFillOpacityF    = 0.1

    vals := ind( CWV(t,:).ge.50 )

    y1 = 0
    y2 = 1e3
    x1 = min(lat(vals)) 
    x2 = max(lat(vals))
    py = (/y1,y2,y2,y1,y1/)
    px = (/x2,x2,x1,x1,x2/)

    dum = gsn_add_polygon(wks,plot(0),px,py,pgres) 
    ;----------------------------------------
    ; Finalize Figure
    ;----------------------------------------
        pres = True;setres_panel() 
        pres@txString   = date(t)

    gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

    trimPNG(fig_file)

    if debug then exit end if
    
end do
;====================================================================================================
;====================================================================================================
    ifile = odir+"animation_frames/"+fig_stub+".*"

    ofile = odir+fig_stub+".gif"

    delay = 20

    system("convert -delay "+delay+" -loop 0 -density 1000 "+ifile+" "+ofile)
    
    printline()
    print("")
    print(""+ofile)
    print("")
    printline()
;====================================================================================================
;====================================================================================================
end

