#!/usr/bin/env python2.7
#===========================================================================================
# This script retrieves ECMWF Interim ReAnalysis (ERAi) data. Files are put in the 
# ./raw_grib/ folder. Each year of data will consist of 4 files, one for each time of day
# available. The ncl script mk.yearly_files.from_grib.ncl will combine data into daily
# means and write out netCDF files.
#
#   documentation on parameter codes can be found at: 
#   http://old.ecmwf.int/publications/library/ecpublications/_pdf/era/era_report_series/RS_1_v2.pdf
#
#     Mar, 2013  Walter Hannah       Colorado State University
#===========================================================================================
from __future__ import print_function
from ecmwfapi import ECMWFDataServer
import sys
import os
# import numpy as np
home = os.getenv("HOME")
#===========================================================================================
# Initialize variables
#===========================================================================================
server = ECMWFDataServer()                      # Setup ECMWF connection
data_dir = home+"/Data/Obs/ERAi/raw_grib/"      # where to put grib data
#---------------------------------------------
# start and end years
#---------------------------------------------

start_year = 2000
end_year   = 2000

get_monthly = True

date1 = "0101"
date2 = "1231"

# date1 = "0101"
# date2 = "0131"

# times = ["00"]
times = ["00","06","12","18"]                   # analysis time of day (Zulu)

# time for first year for when download gets cur off
# just comment out to disable 
# first_times = ["06","12","18"]

#---------------------------------------------
# Specify which variabels to fetch
#---------------------------------------------
# var = ["U","V","OMEGA","T","GEO","Ps"]
# var = ["U","V","OMEGA","T","Q","GEO","Ps","CWV","Psl"]
# var = ["U","V","OMEGA"]
var = ["U"]
# var = ["Q","T","GEO"]
# var = ["CWV"]
# var = ["T","GEO","Ps"]
# var = ["SST","Psl","CWV","Ts","Ps"]
# var = ["Us","Vs",]
#===========================================================================================    
# Loop through each variable
#===========================================================================================
for v in xrange(len(var)):
    
    if var[v] == "GEO"   : par = ["129.128","pl"]   # Geopotential
    if var[v] == "T"     : par = ["130.128","pl"]   # Temperature
    if var[v] == "U"     : par = ["131.128","pl"]   # U-Wind
    if var[v] == "V"     : par = ["132.128","pl"]   # V-Wind
    if var[v] == "Q"     : par = ["133.128","pl"]   # Specific Humidity
    if var[v] == "OMEGA" : par = ["135.128","pl"]   # Vertical Pressure Velocity
    if var[v] == "DIV"   : par = ["155.128","pl"]   # Divergence
    if var[v] == "CLDLIQ": par = ["246.128","pl"]   # Cloud Liquid Water
    if var[v] == "Ps"    : par = ["134.128","sfc"]  # Surface Pressure
    if var[v] == "Ts"    : par = ["235.128","sfc"]  # Surface temperature (skin temperature)
    if var[v] == "SST"   : par = [ "34.128","sfc"]  # Sea Surface Temperature
    if var[v] == "Us"    : par = ["165.128","sfc"]  # Surface U-wind
    if var[v] == "Vs"    : par = ["166.128","sfc"]  # Surface V-wind
    if var[v] == "TCW"   : par = ["136.128","sfc"]  # Total Column Water 
    if var[v] == "CWV"   : par = ["137.128","sfc"]  # Total Column Water Vapor
    if var[v] == "Psl"   : par = ["151.128","sfc"]  # Sea Level Pressure
    
    
    #if var[v] == "blh"   : par = ["159.128","sfc"]  # Boundary layer height
    
    print("")
    print("   ----------------------------------")
    print("   ----------------------------------")
    print("    var    => "+str(var[v]) )
    print("    par    => "+str(par[0]) )
    print("  ----------------------------------")
    print("  ----------------------------------")
    
    for y in xrange(start_year,end_year+1):

        ttimes = times
        if y==start_year and 'first_times' in locals() : ttimes = first_times

        for t in xrange(len(ttimes)):
            if get_monthly :
                date =   str(y)+"0101"+"/"+str(y)+"0201"+"/"+str(y)+"0301"+"/"+str(y)+"0401"+"/"+str(y)+"0501"+"/"+str(y)+"0601"+"/" \
                        +str(y)+"0701"+"/"+str(y)+"0801"+"/"+str(y)+"0901"+"/"+str(y)+"1001"+"/"+str(y)+"1101"+"/"+str(y)+"1201"
            else :
                #date   = str(y)+"0101/to/"+str(y)+"1231"
                date   = str(y)+date1+"/to/"+str(y)+date2
            time   = ttimes[t]
            if par[1]=="sfc" :
                domain = "90/-180/-90/179"
            else:
                domain = "40/-180/-40/179"
            
            # lev = "30/50/70/100/125/150/175/200/250/300/400/500/600/650/700/750/800/825/850/875/900/925/950/975/1000"  
            lev = "70/100/150/200/300/400/500/600/700/750/800/825/850/875/900/925/950/975/1000"  
                   # 70/100/125/150/175/200/225/250/300/350/400/450/500/550/600/650/700/750/775/800/825/850/875/900/925/950/975/1000

            
            if get_monthly : 
                domain = "90/-180/-90/179"
                stream = "mnth"
                file_prefix = "ERAi.1.0-deg.monthly."
            else :
                stream = "oper"
                file_prefix = "ERAi.1.0-deg."
                
            target = data_dir+file_prefix+var[v]+"."+time+"."+str(y)+".grib"
            #-------------------------------------------------
            #-------------------------------------------------
            print("")
            print("     time   => "+str(time  ) )
            print("     date   => "+str(date  ) )
            print("     domain => "+str(domain) )
            print("     target => "+str(target) )
            print("")
            #-------------------------------------------------
            #-------------------------------------------------
            server.retrieve({
                'grid'      : "1/1",
                # 'grid'      : "0.75/0.75",
                'dataset'   : "interim",
                'stream'    : stream,
                'step'      : "0",
                "expver"    : "1",
                'param'     : par[0],
                'levtype'   : par[1], 
                'levelist'  : lev,
                'area'      : domain,
                'time'      : str(time+":00:00"),
                'date'      : date,
                'type'      : "an",
                'class'     : "ei",
                'target'    : target
            })
            #'format'    : "netcdf",
#===========================================================================================
#===========================================================================================
