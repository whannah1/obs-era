#!/usr/bin/env python
#===========================================================================================
# This script retrieves ECMWF Interim ReAnalysis (ERAi) data. Files are put in the 
# ./raw_grib/ folder. Each year of data will consist of 4 files, one for each time of day
# available. The ncl script mk.yearly_files.from_grib.ncl will combine data into daily
# means and write out netCDF files.
#
#    Mar, 2013  Walter Hannah       Colorado State University
#===========================================================================================
from ecmwfapi import ECMWFDataServer
import sys
import os
# import numpy as np
home = os.getenv("HOME")
#===========================================================================================
# Initialize variables
#===========================================================================================
server = ECMWFDataServer()                      # Setup ECMWF connection
data_dir = home+"/Data/Obs/ERAi/raw_grib/"      # where to put grib data
#---------------------------------------------
# start and end years
#---------------------------------------------

start_year = 2000
end_year   = 2009

date1 = "0101"
date2 = "1231"

# overwrite = True

#---------------------------------------------
# time (UTC) & Forecast hour (i.e. step)
#---------------------------------------------
times = ["00","00","12","12"]
steps = ["06","12","06","12"]

# times = ["12"]
# steps = ["12"]
#---------------------------------------------
# Specify which variabels to fetch
#---------------------------------------------

# var = ["TNSW","TNLW","TNSWC","TNLWC"]
var = ["SNSW","SNLW"]

# var = ["TAUX","LHF","TNSW","TNLW","SHF"]
# var = ["LHF","SHF","TNSW","TNLW","SNSW","SNLW"]
#var = ["TNLW","LHF","SHF"]
#var = ["CLDLIQ"]

#===========================================================================================
# Loop through each variable
#===========================================================================================
for v in xrange(len(var)):
    
    if var[v] == "SHF"   : par = ["146.128"]        # Surface Sensible Heat FLux
    if var[v] == "LHF"   : par = ["147.128"]        # Surface Latent Heat Flux
    if var[v] == "SNSW"  : par = ["176.128"]        # Surface Net Shortwave
    if var[v] == "SNLW"  : par = ["177.128"]        # Surface Net Longwave
    if var[v] == "TNSW"  : par = ["178.128"]        # Top Net Shortwave
    if var[v] == "TNLW"  : par = ["179.128"]        # Top Net Longwave
    if var[v] == "TNSWC" : par = ["208.128"]        # Top Net Shortwave - Clearsky
    if var[v] == "TNLWC" : par = ["209.128"]        # Top Net Longwave - Clearsky
    #if var[v] == "CLDLIQ": par = ["246.128","sfc"]  # Cloud Liquid Water
    if var[v] == "PRECT" : par = ["228.128"]        # Total Precip
    if var[v] == "TAUX"  : par = ["180.128","sfc"]  # Eastward turbulent surface stress
    if var[v] == "TAUY"  : par = ["181.128","sfc"]  # Northward turbulent surface stress
    
    print("")
    print("   ----------------------------------")
    print("   ----------------------------------")
    print("    var    => "+str(var[v]) )
    print("    par    => "+str(par[0]) )
    print("  ----------------------------------")
    print("  ----------------------------------")
    
    for y in xrange(start_year,end_year+1):

        # times = ["00","00","12","12"]
        # steps = ["06","12","06","12"]
        # if y==1991 :
        #     times = ["00","00","12","12"]
        #     steps = ["06","12","06","12"]


        for t in xrange(len(times)):
            #date   = str(y)+"0101/to/"+str(y)+"1231"
            date   = str(y)+date1+"/to/"+str(y)+date2
            time   = times[t]
            step   = steps[t]
            fxtm   = "%02d" % ( int(times[t])+int(steps[t]) )
            # domain = "60/-180/-30/179"
            domain = "90/-180/-90/179"
            target = data_dir+"ERAi.1.0-deg."+var[v]+"."+fxtm+"."+str(y)+".grib"
            if os.path.isfile(target) : continue

            stream = "oper"
            # stream = "mnth"
            #-------------------------------------------------
            #-------------------------------------------------
            print("")
            print("     time   => "+str(time  ) )
            print("     date   => "+str(date  ) )
            print("     domain => "+str(domain) )
            print("     target => "+str(target) )
            print("")
            #-------------------------------------------------
            #-------------------------------------------------
            server.retrieve({
                'grid'      : "1/1",
                'dataset'   : "interim",
                'stream'    : stream,
                'step'      : step,
                'type'      : "fc",
                'param'     : par[0],
                'levtype'   : "sfc", 
                'area'      : domain,
                'time'      : time,
                'date'      : date,
                'target'    : target, 
            })
#===========================================================================================
#===========================================================================================
